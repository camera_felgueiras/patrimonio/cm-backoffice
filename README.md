# BackOffice

This project serves as a frontend to control the information of this [API](https://gitlab.com/camera_felgueiras/patrimonio/cm-api). The framework that the project uses is [react](https://reactjs.org/) and [Material UI v.4](https://material-ui.com/).

# Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
3. [Credits](#credits)
4. [License](#license)

---

# Installation <a name="installation"></a>

**Attention** To run this project perfectly clone and install the [API](https://gitlab.com/camera_felgueiras/patrimonio/cm-api)

:warning: The folder name has to be in lowercase.

1. Clone the Repo using `git clone git@gitlab.com:camera_felgueiras/patrimonio/cm-backoffice.git <FolderName>`
2. Cd into the repo folder `cd <FolderName>`
3. Install node dependencies `npm install`
4. Start the development mode `npm start`

If you changed the _API_ **_PORT_** you need to open de file `src/helpers/configs.js` and edit the `apiUrl`.

For production use `npm run build` see [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# Usage <a name="usage"></a>

Learn more:
- [React router](https://reacttraining.com/react-router/)
- [Material UI](https://material-ui.com/)
- [Redux](https://react-redux.js.org/)

## Using The Alert <a name="snack"></a>

To use the alert there is a redux action to trigger the appearance of a SnackBar `alert.action.js` using the alert is very simple takes only une value a `message`, there are for types of SnackBars:

- Error
- Warning
- Info
- Success

Usage Example:

```javascript
import { alertActions } from '../../store/actions/alert.actions';
(...)
alertError('This is a error message');
alertWarning('This is a warning message');
alertInfo('This is a info message');
alertSuccess('This is an alert success');
(...)
const mapActionToProps = {
  alertError: alertActions.error,
  alertWarning: alertActions.warning,
  alertInfo: alertActions.info,
  alertSuccess: alertActions.success,
};
```

## Using an dialog to confirm something

This open a dialog box to confirm receives the following props:

- open -> true or false (to open close)
- passRequired -> true or false (asks for password)
- handleClose -> a function that handles what happens when the closes the dialog
- handlePassword -> required if passRequired true
- title -> the title to show
- desc -> a message to show in the body

See more in `src/components/alert/AlertConfirm.js` propTypes

```javascript
import AlertConfirm from '../alert/AlertConfirm';
(...)
  <AlertConfirm
    open={open}
    passRequired
    handleClose={this.handleClose}
    handlePassword={this.handleInputs}
    title='Validação'
    desc='A password é necessária para poder atualizar o perfil.'
  />
(...)
```

## Using the alert component

This opens a dialog box with another component inside and receives the following props:

- open -> true or false (to open close)
- fullWidth -> true of false (use all space)
- maxWidth -> the max size of the alert ('xs', 'sm', 'md', 'lg', 'xl', false)
- handleClose -> a function that handles what happens when the closes the dialog
- title -> the title to show
- component -> the component to use inside of the dialog
- values -> values used by the component inside of the dialog

See more in `src/components/alert/AlertComponent.js` propTypes

```javascript
import AlertComponent from '../../alert/AlertComponent';
(...)
  <AlertComponent
    open={this.state.open}
    fullWidth={true}
    maxWidth='lg'
    handleClose={this.showCreateCategory}
    title='Adicionar Nova Categoria'
    component={FormCategories}
    values={{ noTitle: true }}
  />
(...)
```

## Use Google maps

To get user input use:
```javascript
  <MapContainer handleClick={this.handleClickMap} />
```

To show details use:
```javascript
  <MapContainer lat={lat.value} lng={lng.value} zoom={20} markerName={title.value} />
```

The google maps API can be changed in `src/components/map/MapContainer.js` bottom of the page in

```javascript
export default GoogleApiWrapper({
  apiKey: '<API key here>',
  v: '3',
  language: 'pt-PT',
})(MapContainer);
```

# Credits <a name="credits"></a>

- Main Developer @Zyr0

# License <a name="License"></a>
