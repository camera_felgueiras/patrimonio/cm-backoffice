import React, { Component } from 'react';

import { Grid, Typography } from '@material-ui/core';

/**
 * Used as a error page
 */
export default class NotFound extends Component {
  render() {
    return (
      <Grid container direction="row" justify="center" alignItems="center">
        <Typography variant="h1">Not Found</Typography>
      </Grid>
    );
  }
}
