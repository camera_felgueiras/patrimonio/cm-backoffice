import React from 'react';

import { connect } from 'react-redux';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Navbar from './navigation/Navbar';
import AlertSnackBar, { openSnack } from './alert/AlertSnackBar';
import { ProtectedRoute } from './auth/ProtectedRoute';
import MainNavigationManager from './managers/MainNavigationManager';
import Login from './auth/Login';
import NotFound from './404/NotFound';

import { history } from '../helpers/history';
import { alertActions } from '../store/actions/alert.actions';
import { main_route } from '../helpers/routes';

import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      dispatch(alertActions.clear());
    });
  }

  render() {
    const { alert } = this.props;
    if (alert.message) {
      openSnack({ type: alert.type, message: alert.message });
    }

    return (
      <Router>
        <Navbar />
        <AlertSnackBar />
        <Switch>
          <Route path='/' exact component={Login} />
          <ProtectedRoute path={main_route} component={MainNavigationManager} />
          <Route path='*' component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

export default connect(mapStateToProps)(App);
