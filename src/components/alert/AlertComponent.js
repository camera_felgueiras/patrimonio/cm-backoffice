import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withMobileDialog from '@material-ui/core/withMobileDialog';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

/**
 * Generic alert takes a title and a description.
 *
 * View: propTypes to see the required to work
 */
export class AlertComponent extends Component {
  render() {
    const {
      open,
      fullScreen,
      fullWidth,
      maxWidth,
      handleClose,
      title,
      desc,
      values,
      component: Component,
    } = this.props;

    return (
      <Dialog
        open={open}
        fullScreen={fullScreen}
        onClose={handleClose('NO')}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        aria-labelledby="dialog-title">
        <DialogTitle id="dialog-title">{title}</DialogTitle>
        <DialogContent>
          {desc && <DialogContentText style={{ whiteSpace: 'pre-wrap' }}>{desc}</DialogContentText>}
          <Component values={values} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose('NO')} color="primary">
            CANCEL
          </Button>
          <Button onClick={handleClose('OK')} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AlertComponent.propTypes = {
  open: PropTypes.bool.isRequired,
  fullScreen: PropTypes.bool.isRequired,
  fullWidth: PropTypes.bool.isRequired,
  maxWidth: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl', false]).isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  component: PropTypes.object.isRequired,
  desc: PropTypes.string,
  values: PropTypes.any,
};

export default withMobileDialog()(AlertComponent);
