import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';

/**
 * Generic alert for confirmations password or no password
 *
 * View: propTypes to see the required to work
 */
class AlertConfirm extends Component {
  render() {
    const { fullScreen, open, passRequired, title, desc, handleClose, handlePassword } = this.props;
    return (
      <Dialog
        open={open}
        fullScreen={fullScreen}
        onClose={handleClose('NO')}
        aria-labelledby="dialog-title">
        <DialogTitle id="dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ whiteSpace: 'pre-wrap' }}>{desc}</DialogContentText>
          {passRequired && (
            <TextField
              autoFocus
              margin="normal"
              name="password"
              label="Password"
              type="password"
              onChange={handlePassword(v => v)}
              fullWidth
            />
          )}
        </DialogContent>
        <DialogActions>
          {!passRequired ? (
            <React.Fragment>
              <Button onClick={handleClose('NO')} color="primary">
                NÃO
              </Button>
              <Button onClick={handleClose('OK')} color="primary">
                SIM
              </Button>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Button onClick={handleClose('NO')} color="primary">
                Cancel
              </Button>
              <Button onClick={handleClose('OK')} color="primary">
                OK
              </Button>
            </React.Fragment>
          )}
        </DialogActions>
      </Dialog>
    );
  }
}

AlertConfirm.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(AlertConfirm);
