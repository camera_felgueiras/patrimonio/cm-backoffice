import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { connect } from 'react-redux';
import { userActions } from '../../store/actions/user.actions';

import { Redirect } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';

import AccountCircle from '@material-ui/icons/AccountCircle';
import LockItem from '@material-ui/icons/Lock';

const styles = theme => ({
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

/**
 * A login screen
 */
class Login extends Component {
  constructor(props) {
    super(props);

    document.title = 'Login';

    if (this.props.loggedIn) {
      this.props.dispatch(userActions.logout());
    }

    this.state = {
      email: '',
      password: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email && password) {
      dispatch(userActions.login(email, password));
    }
  }

  render() {
    const { loggedIn, loading, classes } = this.props;

    if (loggedIn) {
      return <Redirect to="/app" />;
    }

    const emailForm = (
      <TextField
        label="Email"
        name="email"
        type="email"
        onChange={this.handleChange}
        fullWidth
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          ),
        }}
      />
    );

    const passwordForm = (
      <TextField
        style={{ marginTop: '1em' }}
        label="Password"
        name="password"
        type="password"
        onChange={this.handleChange}
        fullWidth
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <LockItem />
            </InputAdornment>
          ),
        }}
      />
    );

    return (
      <Grid container direction="row" justify="center">
        <Grid item xs={12} sm={8} md={5}>
          <Paper className="paper-card">
            <Grid container direction="row" justify="center" alignItems="center">
              <Grid container direction="row" spacing={8} justify="center">
                <Grid item>
                  <Typography variant="h3" gutterBottom>
                    Login
                  </Typography>
                </Grid>
              </Grid>
              <form name="form" onSubmit={this.handleSubmit}>
                {emailForm}
                {passwordForm}
                <Grid container direction="row" spacing={8} justify="flex-end">
                  <Grid item className={classes.wrapper}>
                    <Button
                      className="button_flat"
                      variant="contained"
                      color="secondary"
                      disabled={loading}
                      type="submit">
                      Login
                      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  const { loggedIn, loading } = state.authentication;
  return {
    loggedIn,
    loading,
  };
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(withStyles(styles)(Login));
