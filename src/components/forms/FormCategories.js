import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { typeActions } from '../../store/actions/type.actions';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';

import RemoveIcon from '@material-ui/icons/Remove';

import { titleValidation } from '../../helpers/validator';
import { categoriesActions } from '../../store/actions/categories.actions';

const categoryNameHelper = 'Nova categoria';

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(2),
  },
  flex_left: {
    display: 'flex',
    alignItem: 'flex-end',
    justifyContent: 'flex-end',
  },
  progress: {
    position: 'absolute',
  },
  icon_remove: {
    marginRight: theme.spacing(1),
    color: red[500],
    '&:hover': {
      backgroundColor: red[100],
    },
  },
});

/**
 * A form for categories
 */
class FormCategories extends Component {
  state = {
    typeVal: null,
    name: {
      error: false,
      value: '',
      helper: categoryNameHelper,
    },
  };

  componentDidMount() {
    this.props.updateListTypes();
    this.props.updateListCategories();
  }

  static getDerivedStateFromProps(prop, state) {
    if (prop.updateList) {
      prop.updateListCategories();
    }
    return null;
  }

  getType = id_type => {
    const { listTypes } = this.props;
    const value = listTypes.find(v => v.id === id_type);
    if (value !== undefined) {
      return value.name;
    } else {
      return 'Erro';
    }
  };

  handleTypeChange = e => {
    this.setState({
      typeVal: e.target.value,
    });
  };

  _delete = id => e => {
    e.preventDefault();
    this.props.deleteCategory(id);
  };

  submitForm = e => {
    e.preventDefault();
    const type = this.state.typeVal || this.props.firstVal;
    this.props.createCategory(this.state.name.value, type);
  };

  handleChange = (type, fn, helper, error) => e => {
    this.setState({
      [type]: fn(e.target.value, helper, error),
    });
  };

  render() {
    const {
      classes,
      style,
      firstVal,
      listTypes,
      loadingList,
      loadingCategory,
      listCategories,
      values,
    } = this.props;

    const { noTitle } = values;
    const { typeVal, name } = this.state;

    const filterFrom = (
      <Grid container direction="row" justify="center" alignItems="center">
        <form style={{ width: '100%' }} onSubmit={this.submitForm}>
          <Grid item xs={12}>
            <TextField
              margin="normal"
              label="Nome"
              name="name"
              error={name.error}
              defaultValue={name.value}
              helperText={name.helper || categoryNameHelper}
              onChange={this.handleChange(
                'name',
                titleValidation,
                categoryNameHelper,
                'Nome Invalido',
              )}
              fullWidth
              required
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              select
              fullWidth
              name="tipo"
              label="Select"
              className={classes.textField}
              value={typeVal || firstVal}
              onChange={this.handleTypeChange}
              helperText="Seleciona o tipo"
              margin="normal"
              required>
              {listTypes.map(option => (
                <MenuItem key={option.id} value={option.id}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} className={classes.flex_left}>
            <Button color="primary" type="submit" aria-label="Adicionar">
              Adicionar nova categoria
            </Button>
          </Grid>
        </form>
      </Grid>
    );

    const typeListing = (
      <Grid container direction="row" justify="center" alignItems="center">
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center">Nome</TableCell>
              <TableCell align="center">Tipo</TableCell>
              <TableCell align="center">Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listCategories.map(v => (
              <TableRow key={v.id}>
                <TableCell align="center" component="th" scope="row">
                  {v.name}
                </TableCell>
                <TableCell align="center">{this.getType(v.id_type)}</TableCell>
                <TableCell align="center">
                  <Tooltip title="Remover Filtro" arial-label="Remover">
                    <IconButton className={classes.icon_remove} onClick={this._delete(v.id)}>
                      <RemoveIcon />
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
    );

    return (
      <Grid
        container
        direction="row"
        spacing={2}
        justify="center"
        alignItems="center"
        style={style}>
        <Grid item xs={12} sm={12} md={8}>
          <Paper elevation={4} className={classes.paper}>
            {!noTitle && (
              <Typography variant="h3" align="center">
                Adicionar Categoria
              </Typography>
            )}
            {filterFrom}
          </Paper>
          <Paper elevation={4} className={classes.paper}>
            {typeListing}
          </Paper>
        </Grid>
        {(loadingList || loadingCategory) && (
          <CircularProgress className={classes.progress} size={65} />
        )}
      </Grid>
    );
  }
}

FormCategories.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const { listTypes, firstVal } = state.type;
  const { listCategories, updateList } = state.category;

  return {
    listTypes,
    firstVal,
    updateList,
    loadingList: state.type.loading,
    listCategories: listCategories,
    loadingCategory: state.category.loading,
  };
};

const mapActionsToProps = {
  updateListTypes: typeActions.getAll,
  updateListCategories: categoriesActions.getAll,
  createCategory: categoriesActions.create,
  deleteCategory: categoriesActions.delete,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormCategories));
