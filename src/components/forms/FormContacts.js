import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import {
  phoneValidation,
  emailValidation,
  websiteValidation,
  addressValidation,
} from '../../helpers/validator';

const phoneHelper = 'O numero de telemóvel do local';
const emailHelper = 'O email do local';
const websiteHelper = 'O website do local';
const addressHelper = 'O morada do local';

/**
 * A form with basic contacts
 * Phone, email, website and address, with validation.
 */
export class FormContacts extends Component {
  render() {
    const { handleChange } = this.props;
    const { phone, email, website, address } = this.props.values;

    return (
      <Grid container spacing={2} direction="row" justify="flex-start" alignItems="center">
        <Grid item xs={12} sm={6}>
          <TextField
            margin="normal"
            label="Telemóvel"
            type="text"
            error={phone.error}
            defaultValue={phone.value}
            helperText={phone.helper || phoneHelper}
            onChange={handleChange('phone', phoneValidation, phoneHelper)}
            fullWidth
          />
          <TextField
            margin="normal"
            label="Email"
            type="text"
            error={email.error}
            defaultValue={email.value}
            helperText={email.helper || emailHelper}
            onChange={handleChange('email', emailValidation, emailHelper)}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            margin="normal"
            label="Website"
            type="text"
            error={website.error}
            defaultValue={website.value}
            helperText={website.helper || websiteHelper}
            onChange={handleChange('website', websiteValidation, websiteHelper)}
            fullWidth
          />
          <TextField
            margin="normal"
            label="Morada"
            type="text"
            error={address.error}
            defaultValue={address.value}
            helperText={address.helper || addressHelper}
            onChange={handleChange('address', addressValidation, addressHelper)}
            fullWidth
          />
        </Grid>
      </Grid>
    );
  }
}

export default FormContacts;
