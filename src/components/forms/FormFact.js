import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { alertActions } from '../../store/actions/alert.actions';
import { timelineActions } from '../../store/actions/timeline.actions';
import { imageActions } from '../../store/actions/images.actions';
import { factActions } from '../../store/actions/fact.actions';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';

import RemoveIcon from '@material-ui/icons/Remove';

import FormImages from './FormImages';
import FormRelated from './FormRelated';

import { titleValidation, descSmallValidation, yearValidation } from '../../helpers/validator';
import { imageService } from '../../services/images.service';

const titleHelper = 'Titulo do evento';
const titleError = 'Titulo Invalido';
const dateHelper = 'Data do evento';
const descHelper = 'Descrição do evento';

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(2),
  },
  flex_left: {
    display: 'flex',
    alignItem: 'flex-end',
    justifyContent: 'flex-end',
  },
  progress: {
    position: 'absolute',
  },
  resize: {
    display: 'block',
    marginRight: 'auto',
    marginLeft: 'auto',
    maxWidth: 'auto',
    height: '50px',
  },
  icon_remove: {
    marginRight: theme.spacing(1),
    color: red[500],
    '&:hover': {
      backgroundColor: red[100],
    },
  },
});

/**
 * A fact form this is a simple form for facts,
 */
class FormFact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: {
        helper: dateHelper,
        value: null,
        error: false,
      },
      title: {
        helper: titleHelper,
        value: null,
        error: false,
      },
      desc: {
        helper: descHelper,
        value: null,
        error: false,
      },
    };
  }

  static getDerivedStateFromProps(prop, state) {
    const fact = prop.fact;
    if (fact) {
      if (!state.title.value || !state.date.value || !state.desc.value) {
        return {
          date: {
            helper: dateHelper,
            value: fact.date,
            error: false,
          },
          title: {
            helper: titleHelper,
            value: fact.title,
            error: false,
          },
          desc: {
            helper: descHelper,
            value: fact.desc,
            error: false,
          },
        };
      }
    }
    return null;
  }

  handleChange = (type, fn, helper, error) => e => {
    this.setState({
      [type]: fn(e.target.value, helper, error),
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { date, desc, title } = this.state;
    const { mainImage, relations, values, fact } = this.props;

    if (!values.listing) {
      if (relations.length > 1) {
        this.props.alertError('Só pode existir uma relação.');
        return;
      } else if (relations.length <= 0) {
        this.props.alertError('Relações são obrigatórias');
        return;
      }
    }

    if (!mainImage) {
      this.props.alertError('Sem Imagem');
      return;
    } else if (
      date.value &&
      !date.error &&
      desc.value &&
      !desc.error &&
      title.value &&
      !title.error
    ) {
      const factObj = {
        date: date.value,
        desc: desc.value,
        title: title.value,
        image: mainImage.url,
        related: relations.map(r => ({ id: r.id, type: r.type }))[0],
      };
      if (values.edit) {
        factObj.id_t_related = fact.id_t_related;
        this.props.updateTimeline(values.id, factObj);
      } else if (!values.listing) {
        this.props.createTimeline(factObj);
        this.props.imageClear();
      } else {
        this.props.addFact(factObj);
        this.props.imageClear();
      }
    } else {
      this.props.alertError('Formulário Incompleto ou com erros');
    }
  };

  handleDelete = fact => () => {
    this.props.removeFact(fact);
  };

  render() {
    const { values, classes, facts } = this.props;
    const { date, desc, title } = this.state;

    const factFrom = (
      <Grid container direction="row" justify="center" alignItems="center">
        <form style={{ width: '100%' }} onSubmit={this.handleSubmit}>
          {values && !values.listing && <FormRelated />}
          <Grid item xs={12}>
            <Grid container direction="row" spacing={2} justify="center" alignItems="center">
              <Grid item xs={12} sm={8} md={9}>
                <TextField
                  margin="normal"
                  label="Titulo"
                  error={title.error}
                  value={title.value || ''}
                  helperText={title.helper}
                  onChange={this.handleChange('title', titleValidation, titleHelper, titleError)}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={4} md={3}>
                <TextField
                  margin="normal"
                  label="Data"
                  error={date.error}
                  value={date.value || ''}
                  helperText={date.helper}
                  onChange={this.handleChange('date', yearValidation, dateHelper)}
                  fullWidth
                  required
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin="normal"
              label="Descrição"
              rows={7}
              error={desc.error}
              value={desc.value || ''}
              helperText={desc.helper}
              onChange={this.handleChange('desc', descSmallValidation, descHelper)}
              multiline
              fullWidth
              required
            />
          </Grid>
          <FormImages justMain edit={values.edit} />
          <Grid item xs={12} className={classes.flex_left}>
            <Button color="primary" type="submit" aria-label="Adicionar">
              {values.edit ? 'Editar facto' : 'Adicionar novo facto'}
            </Button>
          </Grid>
        </form>
      </Grid>
    );

    const typeListing = (
      <Grid container direction="row" justify="center" alignItems="center">
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center">Image</TableCell>
              <TableCell align="center">Titulo</TableCell>
              <TableCell align="center">Data</TableCell>
              <TableCell align="center">Descrição</TableCell>
              <TableCell align="center">Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {facts.map(v => (
              <TableRow key={v.id}>
                <TableCell align="center" component="th" scope="row">
                  <img
                    className={classes.resize}
                    src={imageService.getImageTh(v.image)}
                    alt={v.title}
                  />
                </TableCell>
                <TableCell align="center" component="th">
                  {v.title}
                </TableCell>
                <TableCell align="center">{v.date}</TableCell>
                <TableCell align="center">{v.desc}</TableCell>
                <TableCell align="center">
                  <Tooltip title="Remover Facto" arial-label="Remover">
                    <IconButton className={classes.icon_remove} onClick={this.handleDelete(v)}>
                      <RemoveIcon />
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
    );

    return (
      <Grid container direction="row" spacing={2} justify="center" alignItems="center">
        <Grid item xs={12} sm={12} md={8}>
          <Paper elevation={4} className={classes.paper}>
            {!values.noTitle && (
              <Typography variant="h3" align="center">
                Adicionar Facto
              </Typography>
            )}
            {factFrom}
          </Paper>
          {values && values.listing && (
            <Paper elevation={4} className={classes.paper}>
              {typeListing}
            </Paper>
          )}
        </Grid>
      </Grid>
    );
  }
}

FormFact.propTypes = {
  classes: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const { facts } = state.facts;
  const { mainImage } = state.images;
  const { relations } = state.related;
  const { fact } = state.timeline;

  return {
    facts,
    fact,
    mainImage,
    relations,
  };
};

const mapActionsToProps = {
  createTimeline: timelineActions.add,
  updateTimeline: timelineActions.update,
  alertError: alertActions.error,
  imageClear: imageActions.clear,
  addFact: factActions.add,
  removeFact: factActions.delete,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormFact));
