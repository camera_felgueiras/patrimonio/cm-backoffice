import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { imageActions } from '../../store/actions/images.actions';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';

import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import { imageService } from '../../services/images.service';
import { CardActionArea } from '@material-ui/core';

const styles = theme => ({
  card: {
    height: 230,
  },
  media: {
    height: 150,
  },
  icon_remove: {
    margin: theme.spacing(1),
    color: red[500],
    '&:hover': {
      color: theme.palette.getContrastText(red[500]),
      backgroundColor: red[500],
    },
  },
  icon_add: {
    margin: theme.spacing(1),
    color: blue[500],
    '&:hover': {
      color: theme.palette.getContrastText(blue[500]),
      backgroundColor: blue[500],
    },
  },
});

/**
 * A form for images
 */
export class FormImages extends Component {
  handleDeleteImage = (isList, image) => () => {
    this.props.deleteImage(isList, image, this.props.edit);
  };

  handleChange = type => ({ target }) => {
    const files = Array.from(target.files);

    const formData = new FormData();
    files.forEach(file => {
      formData.append(target.name, file);
    });

    this.props.uploadImage(type, formData);
  };

  render() {
    const { mainImage, images, classes, loading, justMain, noMain } = this.props;

    return (
      <Grid
        style={bottom}
        direction="row"
        container
        spacing={8}
        justify="center"
        alignItems="flex-start">
        {loading && (
          <Grid item xs={12} sm={12} md={12}>
            <LinearProgress />
          </Grid>
        )}
        {!noMain && (
          <Grid style={bottom} item xs={12} sm={12} md={justMain ? 12 : 4}>
            {!mainImage ? (
              <Card component="span" raised className={classes.card} style={{ display: 'flex' }}>
                <CardContent style={{ margin: 'auto' }}>
                  <input
                    accept="image/*"
                    id="single-image"
                    name="image"
                    type="file"
                    className="input-gone"
                    onChange={this.handleChange('single')}
                  />
                  <label htmlFor="single-image">
                    <Tooltip title="Upload imagem principal">
                      <IconButton
                        color="primary"
                        aria-label="Upload imagem principal"
                        component="span">
                        <AddIcon />
                      </IconButton>
                    </Tooltip>
                  </label>
                </CardContent>
              </Card>
            ) : (
              <Card raised>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image={imageService.getImage(mainImage.url)}
                    title={mainImage.name}
                  />
                </CardActionArea>
                <CardActions style={{ float: 'right' }}>
                  <IconButton
                    className={classes.icon_remove}
                    onClick={this.handleDeleteImage(false, mainImage.url)}>
                    <DeleteIcon />
                  </IconButton>
                </CardActions>
              </Card>
            )}
          </Grid>
        )}
        {!justMain && (
          <Grid item xs={12} sm={12} md={noMain ? 12 : 8}>
            <Grid container spacing={2} direction="row" alignItems="center" justify="flex-start">
              {images &&
                images.map((tile, index) => (
                  <Grid key={index} item xs={12} sm={6} md={3}>
                    <Card raised>
                      <CardActionArea>
                        <CardMedia
                          className={classes.media}
                          image={imageService.getImage(tile.url)}
                          title={tile.name}
                        />
                      </CardActionArea>
                      <CardActions style={{ float: 'right' }}>
                        <IconButton
                          className={classes.icon_remove}
                          onClick={this.handleDeleteImage(true, tile.url)}>
                          <DeleteIcon />
                        </IconButton>
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
              <Grid item xs={12} sm={6} md={3}>
                <Card component="span" raised className={classes.card} style={{ display: 'flex' }}>
                  <CardContent style={{ margin: 'auto' }}>
                    <input
                      accept="image/*"
                      id="gallery-images"
                      name="images"
                      type="file"
                      className="input-gone"
                      onChange={this.handleChange('multi')}
                      multiple
                    />
                    <label htmlFor="gallery-images">
                      <Tooltip title="Adicionar Foto Relacionada">
                        <IconButton
                          color="primary"
                          aria-label="Adicionar Foto Relacionada"
                          component="span">
                          <AddIcon />
                        </IconButton>
                      </Tooltip>
                    </label>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>
    );
  }
}

const bottom = {
  marginBottom: '2em',
};

FormImages.propTypes = {
  classes: PropTypes.object.isRequired,
  justMain: PropTypes.bool,
};

const mapStateToProps = state => {
  const { images, mainImage, loading } = state.images;

  return {
    images,
    mainImage,
    loading,
  };
};

const mapActionsToProps = {
  deleteImage: imageActions.delete,
  uploadImage: imageActions.upload,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormImages));
