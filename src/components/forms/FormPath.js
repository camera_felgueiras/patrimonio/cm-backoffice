import React, { Component } from 'react';

import { connect } from 'react-redux';
import { pathActions } from '../../store/actions/path.actions';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import red from '@material-ui/core/colors/red';

import AddIcon from '@material-ui/icons/Add';

import Listing from './Listing';

const styles = theme => ({
  bottom: {
    marginBottom: '1em',
  },
  button: {
    margin: theme.spacing(1),
  },
  button_delete: {
    margin: theme.spacing(1),
    color: red[500],
    '&:hover': {
      color: theme.palette.getContrastText(red[500]),
      backgroundColor: red[500],
    },
  },
  root: {
    width: '75%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  table: {
    minWidth: 700,
  },
  resize: {
    display: 'block',
    marginRight: 'auto',
    marginLeft: 'auto',
    maxWidth: '90px',
    height: 'auto',
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

/**
 * Let's user create a path between locals
 */
class FormPath extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  getAllRows = () => {
    return this.props.allRows.filter(f => f !== this.props.path);
  };

  handleDelete = id => () => {
    this.props.removePath(id);
  };

  render() {
    const { classes, loading, path } = this.props;

    return (
      <React.Fragment>
        <span className={classes.wrapper}>
          <Button
            color="primary"
            variant="contained"
            disabled={loading}
            aria-label="Adicionar Relacionado"
            className={classes.button}
            onClick={this.handleOpen}>
            <AddIcon className={classes.extendedIcon} />
            Adicionar Ao Caminho
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
          </Button>
        </span>
        <Dialog
          fullWidth
          maxWidth="lg"
          scroll="paper"
          open={this.state.open}
          onClose={this.handleClose}>
          <DialogContent dividers>
            <Listing
              rows={path}
              update={this.getAllRows}
              allRows={this.getAllRows()}
              add={this.props.addPath}
              searchable
              typeList="add"
            />
          </DialogContent>
        </Dialog>
        {path[0] && (
          <Listing
            rows={path}
            allRows={this.getAllRows()}
            listing
            typeList="del"
            del={this.handleDelete}
          />
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { locals } = state.locals;
  const { path } = state.path;

  let localType = locals;
  localType.map(l => {
    if (l.type === 'Natureza') l.type = 'nature';
    else if (l.type === 'Património Histórico') l.type = 'heritage';
    return l;
  });

  return {
    allRows: localType,
    path,
  };
};

const mapActionsToProps = {
  addPath: pathActions.add,
  removePath: pathActions.delete,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormPath));
