import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { relatedActions } from '../../store/actions/related.actions';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import red from '@material-ui/core/colors/red';

import AddIcon from '@material-ui/icons/Add';

import Listing from './Listing';

const styles = theme => ({
  bottom: {
    marginBottom: '1em',
  },
  button: {
    margin: theme.spacing(1),
  },
  button_delete: {
    margin: theme.spacing(1),
    color: red[500],
    '&:hover': {
      color: theme.palette.getContrastText(red[500]),
      backgroundColor: red[500],
    },
  },
  root: {
    width: '75%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  table: {
    minWidth: 700,
  },
  resize: {
    display: 'block',
    marginRight: 'auto',
    marginLeft: 'auto',
    maxWidth: '90px',
    height: 'auto',
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

/**
 * Let's the user relate personalities, routes and locals
 */
export class FormRelated extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  getAllRows = () => {
    let rows = this.props.allRows.filter(f => f !== this.props.relations);
    if (this.props.edit) {
      const obj = this.props.remove;
      rows = rows.filter(f => f.id !== obj.id && f.desc !== obj.desc);
    }
    return rows;
  };

  handleDelete = id => () => {
    this.props.removeRelated(id);
  };

  render() {
    const { classes, loading, relations } = this.props;

    return (
      <React.Fragment>
        <span className={classes.wrapper}>
          <Button
            color="primary"
            variant="contained"
            disabled={loading}
            aria-label="Adicionar Relacionado"
            className={classes.button}
            onClick={this.handleOpen}>
            <AddIcon className={classes.extendedIcon} />
            Adicionar Relacionado
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
          </Button>
        </span>
        <Dialog
          fullWidth
          maxWidth="lg"
          scroll="paper"
          open={this.state.open}
          onClose={this.handleClose}>
          <DialogContent dividers>
            <Listing
              rows={relations}
              update={this.getAllRows}
              allRows={this.getAllRows()}
              add={this.props.addRelated}
              searchable
              typeList="add"
            />
          </DialogContent>
        </Dialog>
        {relations[0] && (
          <Listing
            rows={relations}
            allRows={this.getAllRows()}
            listing
            typeList="del"
            del={this.handleDelete}
          />
        )}
      </React.Fragment>
    );
  }
}

FormRelated.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const { locals } = state.locals;
  const { relations } = state.related;
  const { routes } = state.routes;
  const { personalities } = state.personalities;

  const personalitiesType = personalities.map(p => {
    p.type = 'personality';
    return p;
  });

  const localsType = locals.map(l => {
    if (l.type === 'Natureza') l.type = 'nature';
    else if (l.type === 'Património Histórico') l.type = 'heritage';
    return l;
  });

  const routeType = routes.map(r => {
    r.type = 'route';
    return r;
  });

  const allRows = personalitiesType.concat(localsType, routeType);

  return {
    routes: routeType,
    locals: localsType,
    personalities: personalitiesType,
    loading: state.locals.loading || state.personalities.loading || state.routes.loading || false,
    relations,
    allRows,
  };
};

const mapActionsToProps = {
  updateRelated: relatedActions.get,
  addRelated: relatedActions.add,
  removeRelated: relatedActions.delete,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormRelated));
