import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { typeActions } from '../../store/actions/type.actions';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';

import RemoveIcon from '@material-ui/icons/Remove';

import { titleValidation } from '../../helpers/validator';

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(2),
  },
  flex_left: {
    display: 'flex',
    alignItem: 'flex-end',
    justifyContent: 'flex-end',
  },
  progress: {
    position: 'absolute',
  },
  wrapper: {
    position: 'relative',
  },
  icon_remove: {
    marginRight: theme.spacing(1),
    color: red[500],
    '&:hover': {
      backgroundColor: red[100],
    },
  },
});

/**
 * A form for the types of locals, routes
 */
class FormTypes extends Component {
  componentDidMount() {
    this.props.updateListTypes();
  }

  static getDerivedStateFromProps(prop, state) {
    if (prop.updateList) {
      prop.updateListTypes();
    }
    return null;
  }

  submitForm = e => {
    e.preventDefault();
    this.props.createType(this.props.name.value);
  };

  _delete = id => e => {
    e.preventDefault();
    this.props.deleteType(id);
  };

  render() {
    const { handleChange, helper, name, classes, style } = this.props;
    const { listTypes, loading } = this.props;

    const typeFrom = (
      <Grid container direction="row" justify="center" alignItems="center">
        <form style={{ width: '100%' }} onSubmit={this.submitForm}>
          <Grid item xs={12}>
            <TextField
              margin="normal"
              label="Nome"
              name="name"
              error={name.error}
              defaultValue={name.value}
              helperText={name.helper}
              onChange={handleChange('typeName', titleValidation, helper, 'Nome Invalido')}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} className={classes.flex_left}>
            <Button color="primary" type="submit" aria-label="Adicionar">
              Adicionar novo tipo
            </Button>
          </Grid>
        </form>
      </Grid>
    );

    const typeListing = (
      <Grid container direction="row" justify="center" alignItems="center">
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center">Nome</TableCell>
              <TableCell align="center">Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listTypes.map(v => (
              <TableRow key={v.id}>
                <TableCell align="center" component="th" scope="row">
                  {v.name}
                </TableCell>
                <TableCell align="center">
                  <Tooltip title="Remover Tipo" arial-label="Remover">
                    <IconButton className={classes.icon_remove} onClick={this._delete(v.id)}>
                      <RemoveIcon />
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
    );

    return (
      <Grid
        container
        direction="row"
        spacing={2}
        justify="center"
        alignItems="center"
        style={style}>
        <Grid item xs={12} sm={12} md={8}>
          <Paper elevation={4} className={classes.paper}>
            <Typography variant="h3" align="center">
              Adicionar Tipo
            </Typography>
            {typeFrom}
          </Paper>
          <Paper elevation={4} className={classes.paper}>
            {typeListing}
          </Paper>
        </Grid>
        {loading && <CircularProgress className={classes.progress} size={65} />}
      </Grid>
    );
  }
}

FormTypes.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const { loading, updateList, listTypes } = state.type;

  return {
    loading,
    updateList,
    listTypes,
  };
};

const mapActionsToProps = {
  createType: typeActions.createType,
  updateListTypes: typeActions.getAll,
  deleteType: typeActions.delete,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(withStyles(styles)(FormTypes));
