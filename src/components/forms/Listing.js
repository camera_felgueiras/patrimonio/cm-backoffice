import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import red from '@material-ui/core/colors/red';
import amber from '@material-ui/core/colors/amber';

import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SearchIcon from '@material-ui/icons/Search';

import { imageService } from '../../services/images.service';
import { qrCodeService } from '../../services/qrCode.service';
import { types, typesPath } from '../../helpers/types.helper';

const edit_btn_color = amber[700];
const delete_btn_color = red[500];

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(3),
    width: '100%',
    overflowX: 'auto',
    marginBottom: theme.spacing(2),
  },
  search: {
    padding: theme.spacing(2),
  },
  table: {
    marginTop: theme.spacing(3),
    marginRight: 'auto',
    marginLeft: 'auto',
    width: '90%',
    maxHeight: '50%',
    minWidth: 650,
  },
  resize: {
    display: 'block',
    marginRight: 'auto',
    marginLeft: 'auto',
    maxWidth: 'auto',
    height: '50px',
  },
  edit_btn: {
    margin: theme.spacing(1),
    color: edit_btn_color,
    '&:hover': {
      color: theme.palette.getContrastText(edit_btn_color),
      backgroundColor: edit_btn_color,
    },
  },
  delete_btn: {
    margin: theme.spacing(1),
    color: delete_btn_color,
    '&:hover': {
      color: theme.palette.getContrastText(delete_btn_color),
      backgroundColor: delete_btn_color,
    },
  },
});

const filter = (allRows, rows, listing, query, type) => {
  let filtered = allRows.filter(v => !rows.includes(v));

  let filteredByType = filtered;
  if (type !== 'all') {
    filteredByType = filtered.filter(v => {
      return type === v.type;
    });
  }

  let newRows;
  newRows = filteredByType.filter(v => {
    let type = v.title || v.name;
    return type.toLowerCase().indexOf(query) !== -1;
  });
  if (!newRows || newRows.length === 0) {
    newRows = filteredByType;
  }
  return newRows;
};

/**
 * A simple component that to list stuff.
 */
class Listing extends React.Component {
  constructor(props) {
    super(props);

    const { rows, allRows, listing, noType } = this.props;

    this.state = {
      rows: filter(allRows, rows, listing, '', 'all', noType),
      query: '',
      type: 'all',
    };
  }

  search = e => {
    const query = e.target.value.toLowerCase();
    const rows = filter(
      this.props.allRows,
      this.props.rows,
      this.props.listing,
      query,
      this.state.type,
      this.props.noType,
    );
    this.setState({
      rows,
      query,
    });
  };

  handlePickerChange = e => {
    const rows = filter(
      this.props.allRows,
      this.props.rows,
      this.props.list,
      this.state.query,
      e.target.value,
      this.state.type,
      this.props.noType,
    );
    this.setState({
      type: e.target.value,
      rows,
    });
  };

  handleAdd = row => () => {
    this.setState({
      rows: this.state.rows.filter(r => r !== row),
    });
    this.props.add(row);
  };

  getType = type => {
    let val;
    if (this.props.path) {
      val = typesPath.find(e => e.value === type);
    } else {
      val = types.find(e => e.value === type);
    }
    if (val) {
      return val.label;
    }
    return 'Erro';
  };

  render() {
    const { classes, listing, typeList, searchable, noType, qr, path, timeline } = this.props;
    const { query, type } = this.state;

    let list = this.props.rows;
    if (!listing) {
      list = this.state.rows;
    }

    const edit = row => (
      <Link to={`${this.props.edit}/${row.id}`}>
        <IconButton className={classes.edit_btn} aria-label="Editar">
          <EditIcon />
        </IconButton>
      </Link>
    );

    const _delete = row => (
      <IconButton
        className={classes.delete_btn}
        aria-label="Delete"
        onClick={this.props.del(row.id)}>
        <DeleteIcon />
      </IconButton>
    );

    const add = row => (
      <IconButton color="primary" aria-label="Adicionar" onClick={this.handleAdd(row)}>
        <AddIcon />
      </IconButton>
    );

    const printList = option => (
      <MenuItem key={option.value} value={option.value}>
        {option.label}
      </MenuItem>
    );

    const search = (
      <div className={classes.search}>
        <Grid container spacing={1} direction="row" justify="center" alignItems="flex-start">
          <Grid item xs={12} md={6}>
            <TextField
              autoFocus
              fullWidth
              value={query}
              name="query"
              type="search"
              label="Pesquisar"
              helperText="Pesquisar por nome"
              onChange={this.search}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          {!noType && (
            <Grid item xs={12} md={3}>
              <TextField
                fullWidth
                name="type"
                select
                label="Tipo"
                value={type}
                onChange={this.handlePickerChange}
                helperText="Filtrar por tipo">
                {path
                  ? typesPath.map(option => printList(option))
                  : types.map(option => printList(option))}
              </TextField>
            </Grid>
          )}
        </Grid>
      </div>
    );

    return (
      <React.Fragment>
        <Paper className={classes.paper}>
          {searchable && search}
          <Table size="small" className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell align="center">
                  <Typography variant="h6">Image</Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography variant="h6">Titulo/Nome</Typography>
                </TableCell>
                {qr && (
                  <TableCell align="center">
                    <Typography variant="h6">QR Code</Typography>
                  </TableCell>
                )}
                {timeline && (
                  <TableCell align="center">
                    <Typography variant="h6">Data</Typography>
                  </TableCell>
                )}
                {!noType && (
                  <TableCell align="center">
                    <Typography variant="h6">Tipo</Typography>
                  </TableCell>
                )}
                {typeList !== 'null' && (
                  <TableCell align="center">
                    <Typography variant="h6">Ações</Typography>
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {list &&
                list.map(row => (
                  <TableRow key={`${row.id} ${row.type} ${row.desc}`}>
                    <TableCell component="th" scope="row">
                      <img
                        className={classes.resize}
                        src={imageService.getImageTh(row.path)}
                        alt={row.title || row.name}
                      />
                    </TableCell>
                    <TableCell align="center">{row.title || row.name}</TableCell>
                    {qr && (
                      <TableCell align="center">
                        <a href={qrCodeService.get(row, 500)} target="_black">
                          <img
                            className={classes.resize}
                            src={qrCodeService.get(row)}
                            alt={`${row.title || row.name} qr code`}
                          />
                        </a>
                      </TableCell>
                    )}
                    {!noType && <TableCell align="center">{this.getType(row.type)}</TableCell>}
                    {timeline && <TableCell align="center">{row.date}</TableCell>}
                    {typeList !== 'null' && (
                      <TableCell align="center">
                        {typeList === 'add' && add(row)}
                        {typeList === 'edit' && edit(row)}
                        {typeList === 'del' && _delete(row)}
                        {typeList === 'del_edit' && (
                          <span>
                            {edit(row)}
                            {_delete(row)}
                          </span>
                        )}
                      </TableCell>
                    )}
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Paper>
      </React.Fragment>
    );
  }
}

Listing.propTypes = {
  classes: PropTypes.object.isRequired,
  rows: PropTypes.array.isRequired,
  typeList: PropTypes.oneOf(['del', 'add', 'edit', 'del_edit', 'null']).isRequired,
  searchable: PropTypes.bool,
  allRows: PropTypes.array,
  listing: PropTypes.bool,
  noType: PropTypes.bool,
  qr: PropTypes.bool,
  timeline: PropTypes.bool,
  path: PropTypes.bool,
  add: PropTypes.func,
};

export default withStyles(styles)(Listing);
