import React, { Component } from 'react';

import { connect } from 'react-redux';
import { alertActions } from '../../store/actions/alert.actions';

import { Route, Switch } from 'react-router-dom';
import { património_route, main_route } from '../../helpers/routes';
import { PatrimónioNavigationManager } from './PatrimónioNavigationManager';
import NotFound from '../404/NotFound';
import UserInfo from '../user/UserInfo';

class AppManager extends Component {
  constructor(props) {
    super(props);
    document.title = 'Informação Utilizador';
  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path={main_route} component={UserInfo} />
          <Route path={património_route} component={PatrimónioNavigationManager} />
          <Route path={`${main_route}/*`} component={NotFound} />
        </Switch>
      </React.Fragment>
    );
  }
}

const mapActionsToProps = {
  alertSuccess: alertActions.success
};

export default connect(
  null,
  mapActionsToProps
)(AppManager);
