import React from 'react';

import { Route } from 'react-router-dom';

import Options from '../património/Options';
import AddLocal from '../património/Local/AddLocal';
import AddPersonality from '../património/Personality/AddPersonality';
import List from '../património/List';
import AddRoute from '../património/Route/AddRoute';
import AddAgenda from '../património/Agenda/AddAgenda';
import AddTimeline from '../património/Timeline/AddTimeline';
import SettingsHeritage from '../settings/SettingsHeritage';

import { património_options_routes as routes } from '../../helpers/routes';

export const PatrimónioNavigationManager = ({ match }) => (
  <React.Fragment>
    <Route exact path={match.url} component={Options} />
    <Route exact path={`${match.url}/settings`} component={SettingsHeritage} />
    {routes.map((option, index) => (
      <React.Fragment key={index}>
        <Route
          path={`${option.url}/add`}
          render={props => {
            if (option.type === 'material') {
              return <AddLocal {...props} title={option.name} type={option.type} />;
            } else if (option.type === 'nature') {
              return <AddLocal {...props} nature title={option.name} type={option.type} />;
            } else if (option.type === 'personality') {
              return <AddPersonality {...props} title={option.name} />;
            } else if (option.type === 'route') {
              return <AddRoute {...props} title={option.name} />;
            } else if (option.type === 'agenda') {
              return <AddAgenda {...props} title={option.name} />;
            } else if (option.type === 'timeline') {
              return <AddTimeline {...props} title={option.name} />;
            }
          }}
        />

        <Route
          path={`${option.url}/edit`}
          render={props => {
            if (option.type === 'material') {
              return <AddLocal {...props} title={option.name} type={option.type} edit />;
            } else if (option.type === 'nature') {
              return <AddLocal {...props} nature title={option.name} type={option.type} edit />;
            } else if (option.type === 'personality') {
              return <AddPersonality {...props} title={option.name} edit />;
            } else if (option.type === 'route') {
              return <AddRoute {...props} title={option.name} edit />;
            } else if (option.type === 'agenda') {
              return <AddAgenda {...props} title={option.name} edit />;
            } else if (option.type === 'timeline') {
              return <AddTimeline {...props} title={option.name} edit />;
            }
          }}
        />

        <Route
          path={`${option.url}/list`}
          render={props => <List {...props} title={option.name} type={option.type} />}
        />
      </React.Fragment>
    ))}
  </React.Fragment>
);
