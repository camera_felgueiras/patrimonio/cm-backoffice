import React from 'react';

import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';

class MapContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      center: {
        lat: props.lat || 41.36457337066573,
        lng: props.lng || -8.198263797586606,
      },
      zoom: props.zoom || 12.75,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
    };
  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });

  onClose = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null,
      });
    }
  };

  handleClick = (mapProps, map, { latLng }) => {
    const lat = latLng.lat();
    const lng = latLng.lng();
    if (!this.props.lat && !this.props.lng) {
      this.props.handleClick(lat, lng);
    }
  };

  render() {
    if (!this.props.loaded) {
      return <div>Loading...</div>;
    }
    return (
      <React.Fragment>
        <div
          style={{
            position: 'relative',
            width: this.props.width || '100%',
            height: this.props.height || '50vh',
          }}>
          <Map
            google={this.props.google}
            zoom={this.state.zoom}
            initialCenter={this.state.center}
            onClick={this.handleClick}>
            {this.props.markerName && (
              <Marker onClick={this.onMarkerClick} name={this.props.markerName} />
            )}
            <InfoWindow
              marker={this.state.activeMarker}
              visible={this.state.showingInfoWindow}
              onClose={this.onClose}>
              <div>
                <h4>{this.state.selectedPlace.name}</h4>
              </div>
            </InfoWindow>
          </Map>
        </div>
      </React.Fragment>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAIpaTrlFX12VG5DRMeElQUXVxiqm9GFeM',
  v: '3',
  language: 'pt-PT',
})(MapContainer);
