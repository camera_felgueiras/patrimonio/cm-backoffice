import React from 'react';

import { connect } from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AndroidIcon from '@material-ui/icons/Android';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

import MenuIcon from '@material-ui/icons/Menu';

import { Link } from 'react-router-dom';
import { património_route } from '../../helpers/routes';

import './Navbar.css';

function HideOnScroll(props) {
  const { children } = props;

  const trigger = useScrollTrigger();

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      selected: 'FelgueirasID',
    };
    this.updateSelected = this.updateSelected.bind(this);
  }

  toggleDrawer = () => () => {
    const status = !this.state.open;
    this.setState({
      open: status,
    });
  };

  updateSelected() {
    document.title = 'Património App';
  }

  render() {
    const { selected } = this.state;
    const { hasLogin, user } = this.props;

    const sideList = (
      <div className="list">
        <List>
          <Link className="text-none" to={património_route} onClick={this.updateSelected}>
            <ListItem button>
              <ListItemIcon>
                <AndroidIcon />
              </ListItemIcon>
              <ListItemText primary="Património App" />
            </ListItem>
          </Link>
          <Divider />
        </List>
      </div>
    );

    const drawer = (
      <IconButton color="inherit" aria-label="Open drawer" onClick={this.toggleDrawer()}>
        <MenuIcon />
      </IconButton>
    );

    return (
      <React.Fragment>
        <HideOnScroll {...this.props}>
          <AppBar>
            <Toolbar>
              {hasLogin && drawer}
              <Typography variant="h6" color="inherit" noWrap>
                {selected}
              </Typography>
            </Toolbar>
          </AppBar>
        </HideOnScroll>
        <Drawer
          role="navigation"
          anchor="left"
          open={this.state.open}
          onClose={this.toggleDrawer()}>
          <Link
            className="drawerHeader MuiNavbar-regular-39"
            to="/app"
            onClick={this.toggleDrawer()}>
            <Grid container alignItems="center">
              <Avatar className="avatar" />
              <Typography variant="h6" noWrap>
                {user && `${user.firstName} ${user.lastName}`}
              </Typography>
            </Grid>
          </Link>
          <Divider />
          <div tabIndex={0} role="button" onClick={this.toggleDrawer()}>
            {sideList}
          </div>
          <Link className="bottom" to="/" onClick={this.toggleDrawer()}>
            <Button variant="contained" color="secondary">
              Logout
            </Button>
          </Link>
        </Drawer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { loggedIn, user } = state.authentication;
  return {
    user,
    hasLogin: loggedIn,
  };
};

export default connect(mapStateToProps)(Navbar);
