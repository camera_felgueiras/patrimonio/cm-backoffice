import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { alertActions } from '../../../store/actions/alert.actions';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import red from '@material-ui/core/colors/red';

import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

import { titleValidation, descValidation } from '../../../helpers/validator';

import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import 'moment/locale/pt';

moment.locale('pt');

const titleHelper = 'Titulo do evento';
const titleError = 'Titulo Invalido';
const descHelper = 'Descrição do evento';

const styles = theme => ({
  paper: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2)
  },
  icon_button: {
    marginRight: theme.spacing(1),
    color: red[500],
    '&:hover': {
      backgroundColor: red[100]
    }
  },
  button_right: {
    marginTop: theme.spacing(2),
    float: 'right'
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class AddAgenda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment(),
      dates: [],
      title: {
        helper: titleHelper,
        value: '',
        error: false
      },
      desc: {
        helper: descHelper,
        value: '',
        error: false
      }
    };

    document.title = 'Adicionar '.concat(this.props.title);
  }

  handleDateChange = date => {
    this.setState({ date });
  };

  handleChange = (type, fn, helper, error) => e => {
    this.setState({
      [type]: fn(e.target.value, helper, error)
    });
  };

  addDate = () => {
    const dates = this.state.dates;
    const currentDate = this.state.date;
    if (!dates.includes(currentDate)) {
      this.setState({
        dates: [...dates, currentDate]
      });
    } else {
      this.props.alertError('Data já existe');
    }
  };

  removeDate = val => () => {
    this.setState(state => ({
      dates: state.dates.filter(v => v !== val)
    }));
  };

  render() {
    const { date, dates, desc, title } = this.state;
    const { classes } = this.props;

    const list = (
      <Grid item xs={12}>
        {dates.length > 0 ? (
          dates.map((val, index) => (
            <Grow in={true} key={index}>
              <Paper className={classes.paper}>
                <Typography variant='body1'>
                  <Tooltip title='Remover Data' arial-label='Remover'>
                    <IconButton className={classes.icon_button} onClick={this.removeDate(val)}>
                      <RemoveIcon />
                    </IconButton>
                  </Tooltip>
                  {moment(val).format('dddd, LL')}{' '}
                </Typography>
              </Paper>
            </Grow>
          ))
        ) : (
          <Typography variant='h4' className={classes.center}>
            Sem datas
          </Typography>
        )}
      </Grid>
    );

    return (
      <MuiPickersUtilsProvider utils={MomentUtils} locale='pt' libInstance={moment}>
        <Grid container direction='row' spacing={2} justify='center' alignItems='flex-start'>
          <Grid item xs={12} md={7}>
            <Grid container direction='row' justify='center' alignItems='center'>
              <Grid item xs={12}>
                <TextField
                  margin='normal'
                  label='Title'
                  error={title.error}
                  defaultValue={title.value}
                  helperText={title.helper}
                  onChange={this.handleChange('title', titleValidation, titleHelper, titleError)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={10} sm={11} md={11}>
                <DatePicker
                  margin='normal'
                  autoOk
                  fullWidth
                  disableToolbar
                  format='dddd, LL'
                  variant='inline'
                  label='Data'
                  value={date}
                  onChange={this.handleDateChange}
                />
              </Grid>
              <Grid item className={classes.center} xs={2} sm={1} md={1}>
                <Tooltip title='Adicionar Data' aria-label='Adicionar'>
                  <IconButton variant='contained' color='primary' onClick={this.addDate}>
                    <AddIcon />
                  </IconButton>
                </Tooltip>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  margin='normal'
                  label='Descrição'
                  rowsMax={10}
                  rows={10}
                  error={desc.error}
                  defaultValue={desc.value}
                  helperText={desc.helper}
                  onChange={this.handleChange('desc', descValidation, descHelper)}
                  multiline
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Button className={classes.button_right} variant='contained' color='primary'>
                  Concluir
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} md={5}>
            <Grid container direction='row' spacing={2} justify='center' alignItems='center'>
              {list}
            </Grid>
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
    );
  }
}

AddAgenda.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, props) => {
  return {
    state,
    props
  };
};

const mapActionToProps = {
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  alertInfo: alertActions.info
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withStyles(styles)(AddAgenda));
