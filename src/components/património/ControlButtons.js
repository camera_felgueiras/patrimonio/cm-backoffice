import React from 'react';

import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import FlagIcon from '@material-ui/icons/Flag';
import HomeIcon from '@material-ui/icons/Home';
import DoneAllIcon from '@material-ui/icons/DoneAll';

/**
 * The top control buttons for the navigation (makes user use mouse)
 */
function ControlButtons(props) {
  const {
    handleNext,
    disableButton,
    handleBack,
    handleStart,
    steps,
    currentStep,
    hasReachedEnd,
  } = props;

  return (
    <div className="right">
      {hasReachedEnd && (
        <Tooltip title={currentStep === 0 ? 'Fim' : 'Inicio'} placement="top">
          <span>
            <IconButton color="primary" onClick={handleStart}>
              {currentStep === 0 ? <FlagIcon /> : <HomeIcon />}
            </IconButton>
          </span>
        </Tooltip>
      )}
      <Tooltip title="Atrás" placement="top">
        <span>
          <IconButton
            variant="outlined"
            color="primary"
            disabled={currentStep === 0}
            onClick={handleBack}>
            <KeyboardArrowLeftIcon />
          </IconButton>
        </span>
      </Tooltip>
      <Tooltip title={currentStep === steps.length ? 'Concluir' : 'Seguinte'} placement="top">
        <IconButton
          variant="contained"
          color="primary"
          disabled={disableButton}
          onClick={handleNext}>
          {currentStep === steps.length ? <DoneAllIcon /> : <KeyboardArrowRightIcon />}
        </IconButton>
      </Tooltip>
    </div>
  );
}

export default ControlButtons;
