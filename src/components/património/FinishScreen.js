import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

import MapContainer from '../map/MapContainer';

import { imageService } from '../../services/images.service';

const styles = theme => ({
  paper: {
    padding: theme.spacing(3, 2),
  },
  spacer: {
    marginTop: theme.spacing(2),
  },
  avatar: {
    margin: 10,
    width: 110,
    height: 110,
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

/**
 * A screen that shows all of the information
 */
class FinishScreen extends Component {
  getName = id => {
    const value = this.props.listCategories.find(v => v.id === id);
    if (value) {
      return value.name;
    }
    return 'Error';
  };

  render() {
    const { values, classes, mainImage, images, facts, relations, path } = this.props;
    const {
      title,
      name,
      job,
      born,
      died,
      lat,
      lng,
      desc,
      phone,
      email,
      categories,
      website,
      address,
      km,
    } = values;

    return (
      <Paper className={classes.paper} elevation={3} style={{ marginTop: '5em' }}>
        <Grid container spacing={2} direction="row" justify="center" alignItems="center">
          {mainImage && (
            <Grid item xs="auto" sm="auto" md={2} className={classes.center}>
              <Avatar
                className={classes.avatar}
                src={imageService.getImageTh(mainImage.url)}
                alt={mainImage.name}
              />
            </Grid>
          )}
          <Grid item xs="auto" sm="auto" md={4}>
            <Typography variant="h5">{(title && title.value) || (name && name.value)}</Typography>
            {job && <Typography variant="subtitle1">{job.value}</Typography>}
            {born && (
              <Typography variant="body1">
                <b>Ano Nascimento:</b> {born.value}
              </Typography>
            )}
            {died && (
              <Typography variant="body1">
                <b>Ano Morte:</b> {died.value}
              </Typography>
            )}
            {lat && (
              <Typography variant="body1">
                <b>Latitude: </b> {lat.value}
              </Typography>
            )}
            {lng && (
              <Typography variant="body1">
                <b>Longitude: </b> {lng.value}
              </Typography>
            )}
            {address && (
              <Typography variant="body1">
                <b>Morada: </b>
                {address.value}
              </Typography>
            )}
            {phone && (
              <Typography variant="body1">
                <b>Telemóvel: </b> {phone.value || 'Sem telemóvel'}
              </Typography>
            )}
            {email && (
              <Typography variant="body1">
                <b>Email: </b>
                {email.value || 'Sem email'}
              </Typography>
            )}
            {website && (
              <Typography variant="body1">
                <b>Website: </b> {website.value || 'Sem website'}
              </Typography>
            )}
            {km && (
              <Typography variant="body1">
                <b>Distancia em km: </b> {km.value}
              </Typography>
            )}
          </Grid>
          {lat && lng && title && (
            <Grid item xs={12} sm={12} md={6}>
              <MapContainer lat={lat.value} lng={lng.value} zoom={20} markerName={title.value} />
            </Grid>
          )}
        </Grid>
        <Grid container spacing={2} direction="column" justify="flex-start" alignItems="flex-start">
          {categories && (
            <React.Fragment>
              <Typography className={classes.spacer} variant="h4">
                Categorias
              </Typography>
              {categories.map((c, index) => (
                <Typography variant="body1" key={index}>
                  {this.getName(c)}
                </Typography>
              ))}
            </React.Fragment>
          )}
          <Typography className={classes.spacer} variant="h4">
            Descrição
          </Typography>
          <Typography variant="body1" paragraph>
            {desc.value}
          </Typography>
          <Typography variant="h4">Imagens</Typography>
          <Grid
            className={classes.spacer}
            container
            spacing={2}
            direction="row"
            justify="center"
            alignItems="center">
            {images.map(v => (
              <Grid className={classes.center} item xs={12} sm={5} md={3} key={v.url}>
                <img src={imageService.getImageTh(v.url)} alt={v.name} />
              </Grid>
            ))}
          </Grid>
          {facts && facts.length > 0 && (
            <React.Fragment>
              <Typography variant="h4" align="center" style={{ marginBottom: '1em' }}>
                Factos
              </Typography>
              <Grid container spacing={3} direction="row" justify="center" alignItems="center">
                {facts.map(v => (
                  <React.Fragment key={v.id}>
                    <Grid item xs={12} sm={2}>
                      <Avatar
                        className={classes.avatar}
                        src={imageService.getImageTh(v.image)}
                        alt={v.title}
                      />
                    </Grid>
                    <Grid item xs={12} sm={10}>
                      <Typography variant="subtitle1">
                        <b>Title:</b> {v.title}
                      </Typography>
                      <Typography variant="subtitle1">
                        <b>Date:</b> {v.date}
                      </Typography>
                      <Typography variant="body1" paragraph>
                        <b>Descrição:</b> {v.desc}
                      </Typography>
                    </Grid>
                  </React.Fragment>
                ))}
              </Grid>
            </React.Fragment>
          )}
          {path && path.length > 0 && (
            <React.Fragment>
              <Typography
                variant="h4"
                align="center"
                style={{ marginBottom: path.length < 1 && '1em' }}>
                Caminho
              </Typography>
              {path.map(v => (
                <React.Fragment key={v.id}>
                  <Grid item xs={12} sm={2}>
                    <Avatar
                      className={classes.avatar}
                      src={imageService.getImageTh(v.path)}
                      alt={v.title}
                    />
                  </Grid>
                  <Grid item xs={12} sm={10}>
                    <Typography variant="subtitle1">
                      <b>Title:</b> {v.title}
                    </Typography>
                    <Typography variant="body1" paragraph>
                      <b>Descrição:</b> {v.desc}
                    </Typography>
                  </Grid>
                </React.Fragment>
              ))}
            </React.Fragment>
          )}
          <Typography
            variant="h4"
            align="center"
            style={{ marginBottom: relations.length < 1 && '1em' }}>
            Relacionados
          </Typography>
          <Grid container spacing={3} direction="row" justify="center" alignItems="center">
            {relations.length > 0 &&
              relations.map(r => (
                <React.Fragment key={r.id}>
                  <Grid item xs={12} sm={2}>
                    <Avatar
                      className={classes.avatar}
                      src={imageService.getImageTh(r.path)}
                      alt={r.name || r.title}
                    />
                  </Grid>
                  <Grid item xs={12} sm={10}>
                    <Typography variant="body1">
                      <b>Nome/Titulo: </b>
                      {r.name || r.title}
                    </Typography>
                  </Grid>
                </React.Fragment>
              ))}
            {relations.length <= 0 && <Typography variant="body1">Sem relacionados</Typography>}
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

FinishScreen.propTypes = {
  classes: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const { mainImage, images } = state.images;
  const { facts } = state.facts;
  const { relations } = state.related;
  const { listCategories } = state.category;
  const { path } = state.path;

  return {
    facts,
    relations,
    mainImage,
    images,
    listCategories,
    path,
  };
};

export default connect(mapStateToProps)(withStyles(styles)(FinishScreen));
