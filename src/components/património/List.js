import React, { Component } from 'react';

import { connect } from 'react-redux';
import { personalityActions } from '../../store/actions/personality.actions';
import { localActions } from '../../store/actions/local.actions';
import { routeActions } from '../../store/actions/route.actions';
import { timelineActions } from '../../store/actions/timeline.actions';

import CircularProgress from '@material-ui/core/CircularProgress';
import Listing from '../forms/Listing';

import { património_options_routes as routes } from '../../helpers/routes';

/**
 * A list component that implements de listing form
 */
class List extends Component {
  componentDidMount() {
    switch (this.props.type) {
      case 'personality':
        this.props.getAllPersonalities();
        break;
      case 'material':
        this.props.getAllLocalsByType('Património Histórico');
        break;
      case 'nature':
        this.props.getAllLocalsByType('Natureza');
        break;
      case 'route':
        this.props.getAllRoutes();
        break;
      case 'timeline':
        this.props.getAllTimeline();
        break;
      case 'qr':
        this.props.getAllPersonalities();
        this.props.getAllLocals();
        break;
      default:
        this.props.getAllPersonalities();
        this.props.getAllLocals();
        break;
    }
  }

  getList = () => {
    switch (this.props.type) {
      case 'personality':
        return this.props.personalities;
      case 'material':
        return this.props.locals;
      case 'nature':
        return this.props.locals;
      case 'route':
        return this.props.routes;
      case 'timeline':
        return this.props.timeline;
      case 'qr':
        return this.props.allRows;
      default:
        return [];
    }
  };

  handleDelete = id => () => {
    switch (this.props.type) {
      case 'personality':
        this.props.deletePersonality(id);
        break;
      case 'material':
        this.props.deleteLocal(id);
        break;
      case 'nature':
        this.props.deleteLocal(id);
        break;
      case 'route':
        this.props.deleteRoute(id);
        break;
      case 'timeline':
        this.props.deleteTimeline(id);
        break;
      default:
        break;
    }
  };

  getLink = () => {
    const type = routes.find(v => v.type === this.props.type);
    if (type) {
      return `${type.url}/edit`;
    }
    return 'Error';
  };

  render() {
    const { p_loading, l_loading, r_loading, t_loading, type } = this.props;

    if (p_loading) return <CircularProgress size={65} />;
    else if (l_loading) return <CircularProgress size={65} />;
    else if (r_loading) return <CircularProgress size={65} />;
    else if (t_loading) return <CircularProgress size={65} />;

    return (
      <Listing
        rows={[]}
        allRows={this.getList()}
        typeList={type !== 'qr' ? 'del_edit' : 'null'}
        searchable
        noType={type !== 'qr'}
        timeline={type === 'timeline'}
        qr={type !== 'timeline'}
        del={this.handleDelete}
        edit={this.getLink()}
      />
    );
  }
}

const mapStateToProps = state => {
  const { personalities } = state.personalities;
  const { locals } = state.locals;
  const { routes } = state.routes;
  const { timeline } = state.timeline;

  const p = personalities.map(p => {
    p.type = 'personality';
    return p;
  });

  const l = locals.map(l => {
    if (l.type === 'Natureza') l.type = 'nature';
    else if (l.type === 'Património Histórico') l.type = 'heritage';
    return l;
  });

  const allRows = p.concat(l);

  const r = routes.map(r => {
    r.type = 'route';
    return r;
  });

  allRows.concat(r);

  return {
    p_loading: state.personalities.loading,
    l_loading: state.locals.loading,
    r_loading: state.routes.loading,
    t_loading: state.timeline.loading,
    personalities: p,
    locals: l,
    routes: r,
    timeline,
    allRows,
  };
};

const mapActionToProps = {
  deletePersonality: personalityActions.delete,
  deleteTimeline: timelineActions.delete,
  deleteLocal: localActions.delete,
  deleteRoute: routeActions.delete,
  getAllTimeline: timelineActions.getAll,
  getAllRoutes: routeActions.getAll,
  getAllPersonalities: personalityActions.getAll,
  getAllLocals: localActions.getAll,
  getAllLocalsByType: localActions.getAllByType,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(List);
