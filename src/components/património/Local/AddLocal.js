import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { connect } from 'react-redux';
import { categoriesActions } from '../../../store/actions/categories.actions';
import { alertActions } from '../../../store/actions/alert.actions';
import { relatedActions } from '../../../store/actions/related.actions';
import { imageActions } from '../../../store/actions/images.actions';
import { factActions } from '../../../store/actions/fact.actions';
import { personalityActions } from '../../../store/actions/personality.actions';
import { localActions } from '../../../store/actions/local.actions';
import { routeActions } from '../../../store/actions/route.actions';
import { pathActions } from '../../../store/actions/path.actions';

import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';

import FormBasicInfo from './FormBasicInfo';
import FormContacts from '../../forms/FormContacts';
import FormImages from '../../forms/FormImages';
import FormRelated from '../../forms/FormRelated';
import FinishScreen from '../FinishScreen';
import AlertConfirm from '../../alert/AlertConfirm';
import ControlButtons from '../ControlButtons';

const styles = theme => ({
  stepper: {
    [theme.breakpoints.down('sm')]: {
      marginLeft: theme.spacing(-1),
      marginTop: theme.spacing(3),
      padding: 0,
    },
  },
});

class AddLocal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      open: false,
      categories: [],
      title: {},
      lat: {},
      lng: {},
      desc: {},
      phone: {},
      email: {},
      website: {},
      address: {},
    };
    let title = 'Adicionar ';
    if (this.props.edit) {
      title = 'Editar ';
      const url = window.location.href.split('/');
      const id = url[url.length - 1];
      this.state.id = id;
    }
    document.title = title.concat(this.props.title);
  }

  componentDidMount() {
    this.updateList();
    this.props.clearLocal();
    this.props.clearImages();
    this.props.clearFacts();
    this.props.clearRelated();
    this.props.clearPath();
    this.props.getAllLocals();
    this.props.getAllPersonalities();
    this.props.getAllRoutes();
    if (this.props.edit) {
      this.props.getLocal(this.state.id);
      this.props.getLocalImages(this.state.id);
      this.props.getLocalRelations(this.state.id);
    }
  }

  static getDerivedStateFromProps(prop, state) {
    const local = prop.local;
    if (local && prop.edit && !state.hasEdit) {
      return {
        title: {
          error: false,
          value: local.title,
        },
        lat: {
          error: false,
          value: local.lat,
        },
        lng: {
          error: false,
          value: local.lng,
        },
        desc: {
          error: false,
          value: local.desc,
        },
        phone: {
          error: false,
          value: local.phone,
        },
        email: {
          error: false,
          value: local.email,
        },
        website: {
          error: false,
          value: local.website,
        },
        address: {
          error: false,
          value: local.address,
        },
        categories: local.categories ? local.categories.map(v => v.id) : [],
      };
    }
    return null;
  }

  updateList = () => {
    if (this.props.nature) {
      this.props.updateListByType(2);
    } else {
      this.props.updateListByType(1);
    }
  };

  getSteps = () => {
    return ['Informação Básica', 'Contactos', 'Imagens', 'Relações'];
  };

  getStepContent = stepIndex => {
    switch (stepIndex) {
      case 0:
        const { title, categories, lat, lng, desc } = this.state;
        const valuesBasicInfo = { title, categories, lat, lng, desc };
        return (
          <FormBasicInfo
            handleChange={this.handleChange}
            handleClickMap={this.handleClickMap}
            values={valuesBasicInfo}
            types={this.props.listCategories}
            updateList={this.updateList}
            edit={this.props.edit}
          />
        );
      case 1:
        const { phone, email, website, address } = this.state;
        const valuesContact = { phone, email, website, address };
        return <FormContacts handleChange={this.handleChange} values={valuesContact} />;
      case 2:
        return <FormImages />;
      case 3:
        return <FormRelated />;
      default:
        return 'Error';
    }
  };

  handleClickMap = (input, val) => {
    this.setState({
      [input]: val,
    });
  };

  handleNext = () => {
    const verify = this.verifyStep();
    if (verify.is) {
      this.props.alertError(verify.message);
    } else {
      const steps = this.getSteps();
      if (this.state.activeStep === steps.length) {
        this.setState(state => ({
          open: !state.open,
        }));
      } else {
        this.setState(state => ({
          nav: this.state.activeStep === steps.length - 1,
          activeStep: state.activeStep + 1,
        }));
      }
    }
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleStart = () => {
    let activeStep = this.state.activeStep;
    if (activeStep === 0) {
      activeStep = this.getSteps().length;
    } else {
      activeStep = 0;
    }
    this.setState({
      activeStep,
    });
  };

  handleChange = (input, validator, helper, error) => e => {
    if (input === 'category') {
      this.setState({
        hasEdit: true,
        categories: e.target.value,
      });
    } else {
      this.setState({
        hasEdit: true,
        [input]: validator(e.target.value, helper, error),
      });
    }
  };

  verifyStep() {
    switch (this.state.activeStep) {
      case 0:
        const { title, lat, lng, desc, categories } = this.state;
        if (
          title.value &&
          !title.error &&
          lat.value &&
          !lat.error &&
          lng.value &&
          !lng.error &&
          desc.value &&
          !desc.error &&
          categories.length > 0
        ) {
          return { is: false };
        }
        if (categories.length >= 0) return { is: true, message: 'Sem categoria' };
        else return { is: true, message: 'Formulário incompleto' };
      case 1:
        const address = this.state.address;
        if (address.value && !address.error) {
          return { is: false };
        }
        return { is: true, message: 'Morada obrigatório' };
      case 2:
        const { mainImage, images } = this.props;
        if (mainImage && images.length > 0) {
          return { is: false };
        }
        return { is: true, message: 'Imagens obrigatórias' };
      case 3:
        return { is: false };
      default:
        return { is: false };
    }
  }

  handleClose = type => () => {
    this.setState({ open: false });
    if (type === 'OK') {
      const local = {
        title: this.state.title.value,
        categories: this.state.categories,
        lat: this.state.lat.value,
        lng: this.state.lng.value,
        desc: this.state.desc.value,
        phone: this.state.phone.value,
        relations: this.props.relations.map(r => ({ id: r.id, type: r.type })),
        email: this.state.email.value,
        website: this.state.website.value,
        address: this.state.address.value,
        facts: this.props.facts,
        images: this.props.images.map(v => v.url),
        mainImage: this.props.mainImage.url,
      };

      if (this.props.edit) {
        this.props.updateLocal(this.state.id, local);
      } else {
        this.props.createLocal(local);
      }
    }
  };

  render() {
    const steps = this.getSteps();
    const { activeStep, open, nav } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <AlertConfirm
          open={open}
          handleClose={this.handleClose}
          title="Confirmar"
          desc={'Tem a certeza que esta tudo certo? '}
        />
        <Typography variant="h3" align="center">
          {document.title}
        </Typography>
        {activeStep !== steps.length && (
          <Stepper className={classes.stepper} activeStep={activeStep} alternativeLabel>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        )}
        <ControlButtons
          steps={steps}
          currentStep={activeStep}
          handleNext={this.handleNext}
          handleBack={this.handleBack}
          handleStart={this.handleStart}
          hasReachedEnd={nav}
        />
        {activeStep === steps.length ? (
          <FinishScreen values={this.state} />
        ) : (
          this.getStepContent(activeStep)
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { listCategories, loading } = state.category;
  const { images, mainImage } = state.images;
  const { relations } = state.related;
  const { facts } = state.facts;
  const { local } = state.locals;

  return {
    images,
    local,
    relations,
    mainImage,
    loading,
    listCategories,
    facts,
  };
};

const mapActionToProps = {
  updateLocal: localActions.update,
  clearLocal: localActions.clear,
  getLocal: localActions.getById,
  getLocalImages: localActions.getImages,
  getLocalRelations: localActions.getRelated,
  createLocal: localActions.create,
  updateListByType: categoriesActions.getByType,
  clearRelated: relatedActions.clear,
  clearFacts: factActions.clear,
  clearImages: imageActions.clear,
  clearPath: pathActions.clear,
  getAllLocals: localActions.getAll,
  getAllPersonalities: personalityActions.getAll,
  getAllRoutes: routeActions.getAll,
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  alertInfo: alertActions.info,
};

AddLocal.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(withStyles(styles)(AddLocal));
