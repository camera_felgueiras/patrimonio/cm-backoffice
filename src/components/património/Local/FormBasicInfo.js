import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import FormCategories from '../../forms/FormCategories';
import FormFact from '../../forms/FormFact';
import AlertComponent from '../../alert/AlertComponent';
import MapContainer from '../../map/MapContainer';

import {
  titleValidation,
  latValidation,
  lngValidation,
  descValidation,
} from '../../../helpers/validator';

const titleHelper = 'O titulo que vai aparecer na lista';
const titleError = 'Titulo Invalido';
const latHelper = 'A latitude do ponto';
const lngHelper = 'A longitude do ponto';
const descHelper = 'A Descrição do que esta a ser adicionado';
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const styles = theme => ({
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
});

class FormBasicInfo extends Component {
  state = {
    open: false,
    alertDates: false,
    categoryName: {
      error: false,
      value: '',
    },
  };

  handleClickMap = (lat, lng) => {
    const latObj = { error: false, value: lat, helper: latHelper };
    const lngObj = { error: false, value: lng, helper: lngHelper };
    this.props.handleClickMap('lat', latObj);
    this.props.handleClickMap('lng', lngObj);
  };

  getName = value => {
    const obj = this.props.types.find(v => v.id === value);
    if (obj.name) {
      return obj.name;
    }
    return 'Erro';
  };

  showCreateCategory = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = type => () => {
    this.setState({ open: false });
    this.setState({ alertDates: false });
    this.props.updateList();
  };

  handleChange = (name, fn, helper, error) => e => {
    this.setState({
      [name]: fn(e.target.value, helper, error),
    });
  };

  openDateDialog = () => {
    this.setState({
      alertDates: true,
    });
  };

  render() {
    const { values, handleChange, types, classes } = this.props;
    const { open, alertDates } = this.state;

    const category = (
      <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
        <Grid item xs={12} sm={10} md={11}>
          <FormControl margin="normal" fullWidth disabled={!types && !types[0]}>
            <InputLabel htmlFor="select-multiple-chip">Categorias</InputLabel>
            <Select
              multiple
              value={values.categories}
              aria-describedby="helper-text"
              onChange={handleChange('category')}
              input={<Input id="select-multiple-chip" />}
              renderValue={selected => (
                <div className={classes.chips}>
                  {selected.map(value => (
                    <Chip
                      key={value}
                      variant="outlined"
                      label={this.getName(value)}
                      className={classes.chip}
                    />
                  ))}
                </div>
              )}
              MenuProps={MenuProps}>
              {types.map(val => (
                <MenuItem key={val.id} value={val.id}>
                  <Checkbox checked={values.categories.indexOf(val.id) > -1} />
                  <ListItemText primary={val.name} />
                </MenuItem>
              ))}
            </Select>
            <FormHelperText id="helper-text">
              A que categoria pretence, isto são os filtros na aplicação.
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={2} md={1}>
          <Tooltip title="Adicionar nova categoria" aria-label="Adicionar nova categoria">
            <IconButton color="primary" onClick={this.showCreateCategory}>
              <OpenInNewIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    );

    return (
      <React.Fragment>
        <AlertComponent
          open={open}
          fullWidth={true}
          maxWidth="lg"
          handleClose={this.handleClose}
          title="Adicionar Nova Categoria"
          component={FormCategories}
          values={{ noTitle: true }}
        />
        <AlertComponent
          open={alertDates}
          fullWidth={true}
          maxWidth="lg"
          handleClose={this.handleClose}
          title="Adicionar Facto"
          desc="Ao adicionar um facto a timeline vai gerada."
          component={FormFact}
          values={{ noTitle: true, listing: true }}
        />
        <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
          <Grid item xs={12} sm={7} md={6}>
            <TextField
              margin="normal"
              label="Titulo"
              type="text"
              error={values.title.error}
              value={values.title.value || ''}
              helperText={values.title.helper || titleHelper}
              onChange={handleChange('title', titleValidation, titleHelper, titleError)}
              fullWidth
            />
            {category}
            <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  margin="normal"
                  label="Latitude"
                  type="text"
                  error={values.lat.error}
                  value={values.lat.value || ''}
                  helperText={values.lat.helper || latHelper}
                  onChange={handleChange('lat', latValidation, latHelper)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  margin="normal"
                  label="Longitude"
                  type="text"
                  error={values.lng.error}
                  value={values.lng.value || ''}
                  helperText={values.lng.helper || lngHelper}
                  onChange={handleChange('lng', lngValidation, lngHelper)}
                  fullWidth
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={5} md={6}>
            <MapContainer handleClick={this.handleClickMap} />
          </Grid>
        </Grid>
        <Grid container spacing={1} direction="row" justify="center" alignItems="center">
          <Grid item xs={12}>
            {!this.props.edit && (
              <Button variant="outlined" color="primary" onClick={this.openDateDialog}>
                Adicionar Facto
              </Button>
            )}
            <TextField
              margin="normal"
              label="Descrição"
              rowsMax={10}
              error={values.desc.error}
              value={values.desc.value || ''}
              helperText={values.desc.helper || descHelper}
              onChange={handleChange('desc', descValidation, descHelper)}
              multiline
              fullWidth
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

FormBasicInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormBasicInfo);
