import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import CardActionArea from '@material-ui/core/CardActionArea';

import AddIcon from '@material-ui/icons/Add';
import ListIcon from '@material-ui/icons/List';
import SettingsIcon from '@material-ui/icons/Settings';

import { património_options_routes as options, património_route } from '../../helpers/routes';

const styles = theme => ({
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    right: theme.spacing(4)
  },
  media: {
    height: 160
  },
  none: {
    textDecoration: 'none'
  }
});

class Options extends Component {
  constructor(props) {
    super(props);
    document.title = 'Opções App Património';
  }

  render() {
    const { classes } = this.props;

    let variant = 'extended';
    let iconClass = classes.extendedIcon;
    if (isWidthDown('sm', this.props.width)) {
      variant = 'round';
      iconClass = null;
    }

    return (
      <Grid container spacing={4} direction='row' justify='center' alignItems='center'>
        {options.map((option, index) => (
          <Grid key={index} item xs={12} sm={6} md={4}>
            <Card raised>
              <CardActionArea>
                <Link to={`${option.url}/add`} className={classes.none}>
                  <CardMedia
                    className={classes.media}
                    image='https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fmoorestown-mall.com%2Fnoimage.gif&f=1'
                    title={option.name}
                  />
                </Link>
                <CardContent>
                  <Typography variant='h6' component='h6' color='textPrimary'>
                    {option.name}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Link to={`${option.url}/list`}>
                  <Tooltip title={`Listar todo de ${option.name}`}>
                    <IconButton aria-label={`Listar todo de ${option.name}`}>
                      <ListIcon />
                    </IconButton>
                  </Tooltip>
                </Link>
                <Link to={`${option.url}/add`}>
                  <Tooltip title={`Adicionar novo ${option.name}`}>
                    <IconButton color='primary' aria-label={`Adicionar novo ${option.name}`}>
                      <AddIcon />
                    </IconButton>
                  </Tooltip>
                </Link>
              </CardActions>
            </Card>
          </Grid>
        ))}
        <Link to={`${património_route}/settings`}>
          <Fab
            variant={variant}
            size='large'
            color='secondary'
            aria-label='Add'
            className={classes.fab}>
            <SettingsIcon className={iconClass} />
            {variant === 'extended' && 'Definições'}
          </Fab>
        </Link>
      </Grid>
    );
  }
}

Options.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withWidth()(Options));
