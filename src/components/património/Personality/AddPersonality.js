import React, { Component } from 'react';

import { connect } from 'react-redux';
import { alertActions } from '../../../store/actions/alert.actions';
import { relatedActions } from '../../../store/actions/related.actions';
import { imageActions } from '../../../store/actions/images.actions';
import { factActions } from '../../../store/actions/fact.actions';
import { personalityActions } from '../../../store/actions/personality.actions';
import { routeActions } from '../../../store/actions/route.actions';
import { localActions } from '../../../store/actions/local.actions';
import { pathActions } from '../../../store/actions/path.actions';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';

import FormBasicInfo from './FormBasicInfo';
import FormImages from '../../forms/FormImages';
import FormRelated from '../../forms/FormRelated';
import AlertConfirm from '../../alert/AlertConfirm';
import FinishScreen from '../FinishScreen';
import ControlButtons from '../ControlButtons';

class AddPersonality extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      open: false,
      name: {},
      job: {},
      born: {},
      died: {},
      desc: {},
    };
    let title = 'Adicionar ';
    if (this.props.edit) {
      title = 'Editar ';
      const url = window.location.href.split('/');
      const id = url[url.length - 1];
      this.state.id = id;
    }
    document.title = title.concat(this.props.title);
  }

  componentDidMount() {
    this.props.clearRelated();
    this.props.clearFacts();
    this.props.clearImages();
    this.props.clearPath();
    this.props.getAllLocals();
    this.props.getAllPersonalities();
    this.props.getAllRoutes();
    if (this.props.edit) {
      this.props.getPersonality(this.state.id);
      this.props.getPersonalityImages(this.state.id);
      this.props.getPersonalityRelated(this.state.id);
    }
  }

  static getDerivedStateFromProps(prop, state) {
    const personality = prop.personality;
    if (personality && prop.edit && !state.hasEdit) {
      return {
        name: {
          error: false,
          value: personality.name,
        },
        job: {
          error: false,
          value: personality.job,
        },
        born: {
          error: false,
          value: personality.birth,
        },
        died: {
          error: false,
          value: personality.death,
        },
        desc: {
          error: false,
          value: personality.desc,
        },
      };
    }
    return null;
  }

  getSteps = () => {
    return ['Informação Básica', 'Imagens', 'Relações'];
  };

  getStepContent = stepIndex => {
    switch (stepIndex) {
      case 0:
        const { name, job, born, died, desc, facts } = this.state;
        const valuesBasicInfo = { name, job, born, died, desc, facts };
        return (
          <FormBasicInfo
            handleChange={this.handleChange}
            values={valuesBasicInfo}
            edit={this.props.edit}
          />
        );
      case 1:
        return <FormImages edit={this.props.edit} />;
      case 2:
        return <FormRelated edit={this.props.edit} remove={this.props.personality} />;
      default:
        return 'Error';
    }
  };

  handleNext = () => {
    const verify = this.verifyStep();
    if (verify.is) {
      this.props.alertError(verify.message);
    } else {
      const steps = this.getSteps();
      if (this.state.activeStep === steps.length) {
        this.setState(state => ({
          open: !state.open,
        }));
      } else {
        this.setState(state => ({
          nav: this.state.activeStep === steps.length - 1,
          activeStep: state.activeStep + 1,
        }));
      }
    }
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleChange = (input, validator, helper, errorHelper, bigger) => e => {
    this.setState({
      hasEdit: true,
      [input]: validator(e.target.value, helper, errorHelper, bigger),
    });
  };

  handleStart = () => {
    let activeStep = this.state.activeStep;
    if (activeStep === 0) {
      activeStep = this.getSteps().length;
    } else {
      activeStep = 0;
    }
    this.setState({
      activeStep,
    });
  };

  handleClose = type => () => {
    this.setState({ open: false });
    if (type === 'OK') {
      const { id, name, job, born, died, desc } = this.state;
      const { mainImage, images, facts, relations } = this.props;

      const personality = {
        name: name.value,
        job: job.value,
        birth: born.value,
        death: died.value,
        desc: desc.value,
        profile_pic: mainImage.url,
        relations: relations.map(v => ({ id: v.id, type: v.type })),
        images: images.map(image => image.url),
        facts,
      };

      if (this.props.edit) {
        this.props.updatePersonality(id, personality);
      } else {
        this.props.createPersonality(personality);
      }
    }
  };

  handleInputs = fn => e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: fn(value),
    });
  };

  verifyStep() {
    switch (this.state.activeStep) {
      case 0:
        const { name, desc, job, born, died } = this.state;
        if (
          name.value &&
          !name.error &&
          desc.value &&
          !desc.error &&
          job.value &&
          !job.error &&
          born.value &&
          !born.error &&
          died.value &&
          !died.error
        ) {
          return { is: false };
        }
        return { is: true, message: 'Formulário Incompleto' };
      case 1:
        const { mainImage, images } = this.props;
        if (mainImage && images.length > 0) {
          return { is: false };
        }
        return { is: true, message: 'Imagens necessárias' };
      case 2:
        return { is: false };
      default:
        return { is: false };
    }
  }

  render() {
    const steps = this.getSteps();
    const { activeStep, open, nav } = this.state;

    return (
      <React.Fragment>
        <AlertConfirm
          open={open}
          handleClose={this.handleClose}
          title="Confirmar"
          desc="Tem a certeza que esta tudo certo?"
        />
        <Typography variant="h3" align="center">
          {document.title}
        </Typography>
        {activeStep !== steps.length && (
          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        )}
        <ControlButtons
          steps={steps}
          currentStep={activeStep}
          handleNext={this.handleNext}
          handleBack={this.handleBack}
          handleStart={this.handleStart}
          hasReachedEnd={nav}
        />
        {activeStep === steps.length ? (
          <FinishScreen values={this.state} />
        ) : (
          this.getStepContent(activeStep)
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { images, mainImage } = state.images;
  const { relations } = state.related;
  const { facts } = state.facts;
  const { personality } = state.personalities;

  return {
    facts,
    relations,
    images,
    mainImage,
    personality,
  };
};

const mapActionToProps = {
  getPersonality: personalityActions.getById,
  getPersonalityImages: personalityActions.getImages,
  getPersonalityRelated: personalityActions.getRelated,
  createPersonality: personalityActions.create,
  updatePersonality: personalityActions.update,
  clearRelated: relatedActions.clear,
  clearFacts: factActions.clear,
  clearImages: imageActions.clear,
  clearPath: pathActions.clear,
  getAllLocals: localActions.getAll,
  getAllPersonalities: personalityActions.getAll,
  getAllRoutes: routeActions.getAll,
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  alertInfo: alertActions.info,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(AddPersonality);
