import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import AlertComponent from '../../alert/AlertComponent';
import FormFact from '../../forms/FormFact';

import {
  titleValidation,
  yearValidation,
  descValidation,
  jobValidation,
} from '../../../helpers/validator';

const nameHelper = 'O nome da personalidade';
const nameHelperError = 'Nome Invalido';
const jobHelper = 'A profissão da personalidade';
const jobHelperError = 'Profissão Invalida';
const bornHelper = 'O ano em que nasceu a personalidade';
const diedHelper = 'O ano em que morreu a personalidade';
const descHelper = 'A Descrição do que esta a ser adicionado';

export default class FormBasicInfo extends Component {
  state = {
    datesOpen: false,
  };

  handleAlertClose = type => () => {
    this.setState({
      datesOpen: false,
    });
  };

  handleAlertOpen = type => () => {
    this.setState({
      [type]: true,
    });
  };

  render() {
    const { datesOpen } = this.state;
    const { values, handleChange } = this.props;

    const formValues = { noTitle: true, listing: true };

    return (
      <React.Fragment>
        <AlertComponent
          open={datesOpen}
          fullWidth={true}
          maxWidth="lg"
          handleClose={this.handleAlertClose}
          title="Adicionar Facto"
          desc="Ao adicionar um facto a timeline vai gerada."
          component={FormFact}
          values={formValues}
        />
        <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
          <Grid item xs={12} sm={6}>
            <TextField
              margin="normal"
              label="Nome"
              type="text"
              error={values.name.error}
              value={values.name.value || ''}
              helperText={values.name.helper || nameHelper}
              onChange={handleChange('name', titleValidation, nameHelper, nameHelperError)}
              fullWidth
              required
            />
            <TextField
              margin="normal"
              label="Profissão"
              type="text"
              error={values.job.error}
              value={values.job.value || ''}
              helperText={values.job.helper || jobHelper}
              onChange={handleChange('job', jobValidation, jobHelper, jobHelperError)}
              fullWidth
              required
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              margin="normal"
              label="Ano Nascimento"
              type="text"
              error={values.born.error}
              value={values.born.value || ''}
              helperText={values.born.helper || bornHelper}
              onChange={handleChange('born', yearValidation, bornHelper, 0)}
              fullWidth
              required
            />
            <TextField
              margin="normal"
              label="Ano Morte"
              disabled={!values.born.value || values.born.error}
              type="text"
              error={values.died.error}
              value={values.died.value || ''}
              helperText={values.died.helper || diedHelper}
              onChange={handleChange('died', yearValidation, diedHelper, values.born.value)}
              fullWidth
              required
            />
          </Grid>
        </Grid>
        <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
          <Grid item xs={12}>
            {!this.props.edit && (
              <Button
                variant="outlined"
                color="primary"
                onClick={this.handleAlertOpen('datesOpen')}>
                Adicionar Facto
              </Button>
            )}
            <TextField
              margin="normal"
              label="Descrição"
              rows={5}
              rowsMax={10}
              error={values.desc.error}
              value={values.desc.value || ''}
              helperText={values.desc.helper || descHelper}
              onChange={handleChange('desc', descValidation, descHelper)}
              multiline
              fullWidth
              required
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}
