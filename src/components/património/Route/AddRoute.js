import React, { Component } from 'react';

import { connect } from 'react-redux';
import { alertActions } from '../../../store/actions/alert.actions';
import { imageActions } from '../../../store/actions/images.actions';
import { personalityActions } from '../../../store/actions/personality.actions';
import { localActions } from '../../../store/actions/local.actions';
import { categoriesActions } from '../../../store/actions/categories.actions';
import { relatedActions } from '../../../store/actions/related.actions';
import { factActions } from '../../../store/actions/fact.actions';
import { routeActions } from '../../../store/actions/route.actions';
import { pathActions } from '../../../store/actions/path.actions';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';
import FormBasicInfo from './FormBasicInfo';
import FormImages from '../../forms/FormImages';
import FormRelated from '../../forms/FormRelated';
import FormPath from '../../forms/FormPath';
import AlertConfirm from '../../alert/AlertConfirm';
import ControlButtons from '../ControlButtons';
import FinishScreen from '../FinishScreen';

const styles = theme => ({
  stepper: {
    [theme.breakpoints.down('sm')]: {
      marginLeft: theme.spacing(-1),
      marginTop: theme.spacing(3),
      padding: 0,
    },
  },
});
class AddPersonality extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasEdit: false,
      open: false,
      activeStep: 0,
      title: {},
      desc: {},
      website: {},
      km: {},
      categories: [],
    };
    let title = 'Adicionar ';
    if (this.props.edit) {
      title = 'Editar ';
      const url = window.location.href.split('/');
      const id = url[url.length - 1];
      this.state.id = id;
    }
    document.title = title.concat(this.props.title);
  }

  updateList = () => {
    this.props.getTypeList(3);
  };

  componentDidMount() {
    this.props.clearImage();
    this.props.clearFacts();
    this.props.clearRelated();
    this.props.clearPath();
    this.props.getAllLocals();
    this.props.getAllPersonalities();
    this.props.getAllRoutes();
    this.updateList();
    if (this.props.edit) {
      this.props.getRoute(this.state.id);
      this.props.getRoutePath(this.state.id);
      this.props.getRouteImages(this.state.id);
      this.props.getRouteRelations(this.state.id);
    }
  }

  static getDerivedStateFromProps(prop, state) {
    const route = prop.route;
    if (route && prop.edit && !state.hasEdit) {
      return {
        title: {
          error: false,
          value: route.title,
        },
        km: {
          error: false,
          value: route.km,
        },
        desc: {
          error: false,
          value: route.desc,
        },
        website: {
          error: false,
          value: route.website,
        },
        categories: route.categories ? route.categories.map(v => v.id) : [],
      };
    }
    return null;
  }

  getSteps = () => {
    return ['Informação Básica', 'Caminho', 'Imagens', 'Relações'];
  };

  getStepContent = stepIndex => {
    const { title, website, desc, km, categories } = this.state;
    const valuesBasicInfo = { title, website, desc, km, categories };

    switch (stepIndex) {
      case 0:
        return (
          <FormBasicInfo
            handleChange={this.handleChange}
            values={valuesBasicInfo}
            types={this.props.listCategories}
            updateList={this.updateList}
          />
        );
      case 1:
        return <FormPath />;
      case 2:
        return <FormImages edit={this.props.edit} />;
      case 3:
        return <FormRelated edit={this.props.edit} remove={this.props.route} />;
      default:
        return 'Error';
    }
  };

  handleNext = () => {
    const verify = this.verifySteps();
    if (verify.is) {
      this.props.alertError(verify.message);
    } else {
      const steps = this.getSteps();
      if (this.state.activeStep === steps.length) {
        this.setState(state => ({
          open: !state.open,
        }));
      } else {
        this.setState(state => ({
          nav: this.state.activeStep === steps.length - 1,
          activeStep: state.activeStep + 1,
        }));
      }
    }
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleStart = () => {
    let activeStep = this.state.activeStep;
    if (activeStep === 0) {
      activeStep = this.getSteps().length;
    } else {
      activeStep = 0;
    }
    this.setState({
      activeStep,
    });
  };

  handleChange = (input, validator, helper, error) => e => {
    if (input === 'category') {
      this.setState({
        hasEdit: true,
        categories: e.target.value,
      });
    } else {
      this.setState({
        hasEdit: true,
        [input]: validator(e.target.value, helper, error),
      });
    }
  };

  handleClose = type => () => {
    this.setState({ open: false });
    if (type === 'OK') {
      const route = {
        title: this.state.title.value,
        desc: this.state.desc.value,
        website: this.state.website.value,
        km: this.state.km.value,
        categories: this.state.categories,
        path: this.props.path.map(v => v.id),
        images: this.props.images.map(v => v.url),
        relations: this.props.relations.map(r => ({ id: r.id, type: r.type })),
        image: this.props.mainImage.url,
      };

      if (this.props.edit) {
        this.props.updateRoute(this.state.id, route);
      } else {
        this.props.createRoute(route);
      }
    }
  };

  verifySteps() {
    switch (this.state.activeStep) {
      case 0:
        const { title, desc, website } = this.state;
        if (title.value && desc.value && website.value) {
          return { is: false };
        }
        return { is: true, message: 'Formulário Incompleto' };
      case 1:
        const { path } = this.props;
        if (path.length > 0) {
          return { is: false };
        }
        return { is: true, message: 'Caminho obrigatório' };
      case 2:
        const { images } = this.props;
        if (images.length > 0) {
          return { is: false };
        }
        return { is: true, message: 'Caminho obrigatório' };
      case 3:
        return { is: false };
      default:
        return { is: false };
    }
  }

  render() {
    const steps = this.getSteps();
    const { activeStep, open, nav } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <AlertConfirm
          open={open}
          handleClose={this.handleClose}
          title="Confirmar"
          desc={'Tem a certeza que esta tudo certo? '}
        />
        <Typography variant="h3" align="center">
          {document.title}
        </Typography>
        {activeStep !== steps.length && (
          <Stepper className={classes.stepper} activeStep={activeStep} alternativeLabel>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        )}
        <ControlButtons
          steps={steps}
          currentStep={activeStep}
          handleNext={this.handleNext}
          handleBack={this.handleBack}
          handleStart={this.handleStart}
          hasReachedEnd={nav}
        />
        {activeStep === steps.length ? (
          <FinishScreen values={this.state} />
        ) : (
          this.getStepContent(activeStep)
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { listCategories, loading } = state.category;
  const { images, mainImage } = state.images;
  const { relations } = state.related;
  const { path } = state.path;
  const { route } = state.routes;

  return {
    images,
    relations,
    mainImage,
    loading,
    listCategories,
    path,
    route,
  };
};

const mapActionToProps = {
  getRoute: routeActions.getById,
  getRouteImages: routeActions.getImages,
  getRouteRelations: routeActions.getRelated,
  getRoutePath: routeActions.getPath,
  updateRoute: routeActions.update,
  clearImage: imageActions.clear,
  clearRelated: relatedActions.clear,
  clearFacts: factActions.clear,
  clearPath: pathActions.clear,
  getAllLocals: localActions.getAll,
  getAllPersonalities: personalityActions.getAll,
  getAllRoutes: routeActions.getAll,
  getTypeList: categoriesActions.getByType,
  createRoute: routeActions.create,
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  alertInfo: alertActions.info,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(withStyles(styles)(AddPersonality));
