import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';

import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import AlertComponent from '../../alert/AlertComponent';
import FormCategories from '../../forms/FormCategories';

import {
  titleValidation,
  descValidation,
  websiteValidation,
  kmValidation
} from '../../../helpers/validator';

const titleHelper = 'O titulo do que esta a ser adicionado';
const titleError = 'Titulo Invalido';
const descHelper = 'A Descrição do que esta a ser adicionado';
const websiteHelper = 'O Website relacionado com a rota';
const kmHelper = 'Os kms que a rota têm';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const styles = theme => ({
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
});

class FormBasicInfo extends Component {
  state = {
    open: false
  };

  getName = value => {
    const obj = this.props.types.find(v => v.id === value);
    if (obj.name) {
      return obj.name;
    }
    return 'Erro';
  };

  showCreateCategory = () => () => {
    this.setState(state => ({
      open: !state.open
    }));
    this.props.updateList();
  };

  render() {
    const { values, types, handleChange, classes } = this.props;

    const category = (
      <Grid container spacing={1} direction='row' justify='flex-start' alignItems='center'>
        {types[0] && (
          <Grid item xs={12} sm={10} md={11}>
            <FormControl margin='normal' fullWidth>
              <InputLabel htmlFor='select-multiple-chip'>Categorias</InputLabel>
              <Select
                multiple
                value={values.categories}
                aria-describedby='helper-text'
                onChange={handleChange('category')}
                input={<Input id='select-multiple-chip' />}
                renderValue={selected => (
                  <div className={classes.chips}>
                    {selected.map(value => (
                      <Chip
                        key={value}
                        variant='outlined'
                        label={this.getName(value)}
                        className={classes.chip}
                      />
                    ))}
                  </div>
                )}
                MenuProps={MenuProps}>
                {types.map(val => (
                  <MenuItem key={val.id} value={val.id}>
                    <Checkbox checked={values.categories.indexOf(val.id) > -1} />
                    <ListItemText primary={val.name} />
                  </MenuItem>
                ))}
              </Select>
              <FormHelperText id='helper-text'>
                A que categoria pretence, isto são os filtros na aplicação.
              </FormHelperText>
            </FormControl>
          </Grid>
        )}
        <Grid item xs={12} sm={2} md={1}>
          <Tooltip title='Adicionar nova categoria' aria-label='Adicionar nova categoria'>
            <IconButton color='primary' onClick={this.showCreateCategory()}>
              <OpenInNewIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    );

    return (
      <React.Fragment>
        <AlertComponent
          open={this.state.open}
          fullWidth={true}
          maxWidth='lg'
          handleClose={this.showCreateCategory}
          title='Adicionar Nova Categoria'
          component={FormCategories}
          values={{ noTitle: true }}
        />
        <Grid container spacing={1} direction='row' justify='flex-start' alignItems='center'>
          <Grid item xs={12} sm={6}>
            <TextField
              margin='normal'
              label='Titulo'
              error={values.title.error}
              value={values.title.value || ''}
              helperText={values.title.helper || titleHelper}
              onChange={handleChange('title', titleValidation, titleHelper, titleError)}
              fullWidth
            />
            {category}
          </Grid>
          <Grid item xs={12} sm={6}>
            <Grid item xs={12}>
              <TextField
                margin='normal'
                label='Km'
                error={values.km.error}
                value={values.km.value || ''}
                helperText={values.km.helper || kmHelper}
                onChange={handleChange('km', kmValidation, kmHelper)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin='normal'
                label='Website'
                error={values.website.error}
                value={values.website.value || ''}
                helperText={values.website.helper || websiteHelper}
                onChange={handleChange('website', websiteValidation, websiteHelper)}
                fullWidth
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin='normal'
              label='Descrição'
              rows={5}
              rowsMax={10}
              error={values.desc.error}
              value={values.desc.value || ''}
              helperText={values.desc.helper || descHelper}
              onChange={handleChange('desc', descValidation, descHelper)}
              multiline
              fullWidth
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

FormBasicInfo.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FormBasicInfo);
