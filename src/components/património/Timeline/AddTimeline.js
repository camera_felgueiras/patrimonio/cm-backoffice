import React, { Component } from 'react';

import { connect } from 'react-redux';
import { alertActions } from '../../../store/actions/alert.actions';
import { relatedActions } from '../../../store/actions/related.actions';
import { imageActions } from '../../../store/actions/images.actions';
import { factActions } from '../../../store/actions/fact.actions';
import { personalityActions } from '../../../store/actions/personality.actions';
import { localActions } from '../../../store/actions/local.actions';
import { routeActions } from '../../../store/actions/route.actions';
import { timelineActions } from '../../../store/actions/timeline.actions';

import FormFact from '../../forms/FormFact';

class AddTimeline extends Component {
  state = { id: -1 };

  constructor(props) {
    super(props);
    let title = 'Adicionar ';
    if (this.props.edit) {
      title = 'Editar ';
      const url = window.location.href.split('/');
      const id = url[url.length - 1];
      this.state = {
        id,
      };
    }
    document.title = title.concat(this.props.title);
  }

  componentDidMount() {
    this.props.clearImages();
    this.props.clearFacts();
    this.props.clearRelated();
    this.props.getAllLocals();
    this.props.getAllPersonalities();
    this.props.getAllRoutes();
    if (this.props.edit) {
      this.props.getTimelineInfo(this.state.id);
      this.props.getTimelineImage(this.state.id);
      this.props.getTimelineRelated(this.state.id);
    }
  }

  render() {
    return (
      <FormFact
        values={{ noTitle: false, listing: false, edit: this.props.edit, id: this.state.id }}
      />
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    state,
    props,
  };
};

const mapActionToProps = {
  getTimelineInfo: timelineActions.getById,
  getTimelineImage: timelineActions.getImage,
  getTimelineRelated: timelineActions.getRelated,
  clearRelated: relatedActions.clear,
  clearFacts: factActions.clear,
  clearImages: imageActions.clear,
  getAllLocals: localActions.getAll,
  getAllPersonalities: personalityActions.getAll,
  getAllRoutes: routeActions.getAll,
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  alertInfo: alertActions.info,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(AddTimeline);
