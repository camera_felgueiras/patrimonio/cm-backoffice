import React, { Component } from 'react';

import { connect } from 'react-redux';
import { alertActions } from '../../store/actions/alert.actions';
import { typeActions } from '../../store/actions/type.actions';

import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import EditIcon from '@material-ui/icons/Edit';
import ListIcon from '@material-ui/icons/List';

// import FormTypes from '../forms/FormTypes';
import FormCategories from '../forms/FormCategories';
import List from '../património/List';

const typeNameHelper = 'Novo tipo';

const buttons = [
  {
    name: 'Editar Categorias',
    type: 'filter',
    icon: <EditIcon />
  },
  /*{
    name: 'Editar tipos',
    type: 'type',
    icon: <EditIcon />
  },*/
  {
    name: 'Ver QR codes',
    type: 'qr',
    icon: <ListIcon />
  }
];

class SettingsHeritage extends Component {
  constructor(props) {
    super(props);
    document.title = 'Definições Património';
    this.state = {
      typeName: {
        error: false,
        value: '',
        helper: typeNameHelper
      },
      showTypeForm: false,
      showCategoryForm: false,
      showQrList: false
    };
  }

  handleButton = type => () => {
    if (type === 'type') {
      this.setState(state => ({
        showTypeForm: !state.showTypeForm,
        showCategoryForm: false,
        showQrList: false
      }));
    } else if (type === 'filter') {
      this.setState(state => ({
        showCategoryForm: !state.showCategoryForm,
        showTypeForm: false,
        showQrList: false
      }));
    } else if (type === 'qr') {
      this.setState(state => ({
        showQrList: !state.showQrList,
        showCategoryForm: false,
        showTypeForm: false
      }));
    }
  };

  handleChange = (type, fn, helper, error) => e => {
    this.setState({
      [type]: fn(e.target.value, helper, error)
    });
  };

  render() {
    const { showCategoryForm, showQrList } = this.state;

    return (
      <React.Fragment>
        <Grid container direction='row' spacing={4} justify='center' alignItems='center'>
          {buttons.map(b => (
            <Grid item xs={12} sm={6} md={3} key={b.type}>
              <Card raised>
                <CardActionArea>
                  <CardContent>
                    <Typography variant='h5' align='center'>
                      {b.name}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <IconButton
                    style={{ margin: 'auto' }}
                    variant='contained'
                    color='primary'
                    onClick={this.handleButton(b.type)}>
                    {b.icon}
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
        {/*showTypeForm && (
          <Fade in={showTypeForm}>
            <FormTypes handleChange={this.handleChange} helper={typeNameHelper} name={typeName} />
          </Fade>
        )*/}
        {showCategoryForm && (
          <Fade in={showCategoryForm}>
            <FormCategories values={{ noTitle: false }} />
          </Fade>
        )}
        {showQrList && (
          <Fade in={showQrList}>
            <List title='Lista QR codes' type='qr' />
          </Fade>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    type: state.type,
    state,
    props
  };
};

const mapActionsToProps = {
  createType: typeActions.createType,
  updateListTypes: typeActions.getAll,
  alertError: alertActions.error,
  alertSuccess: alertActions.success
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(SettingsHeritage);
