import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { alertActions } from '../../store/actions/alert.actions';
import { userActions } from '../../store/actions/user.actions';

import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import blueGrey from '@material-ui/core/colors/blueGrey';

import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';

import AlertConfirm from '../alert/AlertConfirm';

const color = '#' + ((Math.random() * 0xffffff) << 0).toString(16);
const edit_btn_color = blueGrey[700];

const styles = theme => ({
  progress: {
    margin: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    maxWidth: '65%',
    width: 'auto'
  },
  edit_button: {
    margin: theme.spacing(1),
    color: edit_btn_color,
    '&:hover': {
      color: theme.palette.getContrastText(edit_btn_color),
      backgroundColor: edit_btn_color
    }
  },
  avatar: {
    margin: 5,
    width: 60,
    height: 60
  }
});

const firstNameHelper = 'Primeiro nome do utilizador';
const lastNameHelper = 'Ultimo nome do utilizador';
const emailHelper = 'Email do utilizador';

const validateFirstName = value => {
  let helper = firstNameHelper;
  let error = false;
  const reg = /[\w\u0080-\u00FF][\s]/;
  if (reg.test(value)) {
    helper = 'Primeiro nome não pode conter espaço';
    error = true;
  } else if (value.length < 3) {
    helper = 'Primeiro nome pequeno';
    error = true;
  }
  return { error, value, helper };
};

const validateLastName = value => {
  let helper = lastNameHelper;
  let error = false;
  const reg = /[\w\u0080-\u00FF][\s]/;
  if (reg.test(value)) {
    helper = 'Segundo nome não pode conter espaços';
    error = true;
  } else if (value.length < 3) {
    helper = 'Segundo nome pequeno';
    error = true;
  }
  return { error, value, helper };
};

const validateEmail = value => {
  let helper = emailHelper;
  let error = false;
  const reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
  if (!reg.test(value)) {
    helper = 'Email Invalido';
    error = true;
  }
  return { error, value, helper };
};

class UserInfo extends Component {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      open: false,
      email: {
        error: false,
        value: user.email,
        helper: emailHelper
      },
      firstName: {
        error: false,
        value: user.firstName.trim(),
        helper: firstNameHelper
      },
      lastName: {
        error: false,
        value: user.lastName,
        helper: lastNameHelper
      },
      newPassword: {
        error: false,
        value: ''
      },
      newRePassword: {
        error: false,
        value: ''
      },
      showEdit: false
    };
  }

  showEditFields = () => {
    this.setState(state => ({
      showEdit: !state.showEdit
    }));
  };

  handleInputs = fn => e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: fn(value)
    });
  };

  handleSubmit = type => e => {
    e.preventDefault();
    if (
      this.state.email.error ||
      this.state.firstName.error ||
      this.state.lastName.error ||
      this.state.newPassword.error ||
      this.state.newRePassword.error
    ) {
      this.props.alertError('Formulário incompleto ou com erros');
    } else {
      this.setState({ open: true, type });
    }
  };

  handleClose = type => () => {
    this.setState({ open: false });
    if (type === 'OK') {
      const { user, updateUser, updatePassword } = this.props;
      if (this.state.type === 'edit') {
        const { password, email, firstName, lastName } = this.state;
        updateUser(user, {
          id: user.id,
          firstName: firstName.value,
          lastName: lastName.value,
          email: email.value,
          password
        });
      } else {
        const { password, newPassword } = this.state;
        updatePassword({
          id: user.id,
          password,
          newPassword: newPassword.value
        });
      }
    }
  };

  handlePassword = e => {
    const name = e.target.name;
    const value = e.target.value;
    let error = false;
    if (name === 'newRePassword') {
      if (this.state.newPassword.value !== value) {
        error = true;
      }
    }
    this.setState({
      [name]: { error, value }
    });
  };

  render() {
    const { classes, user, loading } = this.props;
    const { showEdit, firstName, lastName, email, open, newPassword, newRePassword } = this.state;

    const editForm = (
      <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            error={firstName.error}
            value={firstName.value}
            name='firstName'
            type='text'
            label='Primeiro nome'
            helperText={firstName.helper}
            onChange={this.handleInputs(validateFirstName)}
            required
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            error={lastName.error}
            value={lastName.value}
            name='lastName'
            type='text'
            label='Ultimo nome'
            helperText={lastName.helper}
            onChange={this.handleInputs(validateLastName)}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            error={email.error}
            value={email.value}
            name='email'
            type='email'
            label='Email do utilizador'
            helperText={email.helper}
            onChange={this.handleInputs(validateEmail)}
            required
          />
        </Grid>

        <Button style={{ marginLeft: 'auto' }} color='primary' type='submit'>
          Atualizar Informação
        </Button>
      </Grid>
    );

    const passForm = (
      <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            name='newPassword'
            type='password'
            error={newPassword.error}
            value={newPassword.value}
            label='Nova password'
            helperText='Nova password'
            onChange={this.handlePassword}
            required
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            name='newRePassword'
            type='password'
            error={newRePassword.error}
            value={newRePassword.value}
            label='Re nova password'
            helperText='Repetir nova password'
            onChange={this.handlePassword}
            required
          />
        </Grid>
        <Button style={{ marginLeft: 'auto' }} color='primary' type='submit'>
          Mudar Password
        </Button>
      </Grid>
    );

    return (
      <Box className={classes.paper} mx='auto' boxShadow={5}>
        <AlertConfirm
          open={open}
          passRequired
          handleClose={this.handleClose}
          handlePassword={this.handleInputs}
          title='Validação'
          desc='A password é necessária para poder atualizar o perfil.'
        />
        <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
          {loading && <CircularProgress className={classes.progress} />}
          <Avatar className={classes.avatar} style={{ backgroundColor: color }}>
            {`${user.firstName.charAt(0)}${user.lastName.charAt(0)}`}
          </Avatar>
          <Grid item>
            <Typography variant='h5' component='h3'>
              {`${user.firstName} ${user.lastName}`}
            </Typography>
            <Typography component='p'>{user.email}</Typography>
          </Grid>
          <Grid item>
            <IconButton className={classes.edit_button} onClick={this.showEditFields}>
              {showEdit ? <CloseIcon /> : <EditIcon />}
            </IconButton>
          </Grid>
        </Grid>
        {showEdit && (
          <React.Fragment>
            <form onSubmit={this.handleSubmit('edit')}>{editForm}</form>
            <form style={{ marginTop: '3em' }} onSubmit={this.handleSubmit('pass')}>
              {passForm}
            </form>
          </React.Fragment>
        )}
      </Box>
    );
  }
}

UserInfo.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, props) => {
  return {
    user: state.authentication.user,
    loading: state.authentication.loading
  };
};

const mapActionsToProps = {
  alertError: alertActions.error,
  alertSuccess: alertActions.success,
  updateUser: userActions.update,
  updatePassword: userActions.password
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withStyles(styles)(UserInfo));
