export const apiUrl = 'http://localhost:8080/api';
export const apiQrCode = 'https://api.qrserver.com/v1/create-qr-code/?';

export function handleResponse(res) {
  return res.text().then(text => {
    const data = text && JSON.parse(text);
    if (!res.ok) {
      const error = (data && data.message) || res.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}
