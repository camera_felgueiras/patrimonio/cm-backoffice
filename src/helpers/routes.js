export const main_route = '/app';
export const património_route = main_route.concat('/património');

const agenda_url = património_route.concat('/agenda');
const rotas_url = património_route.concat('/rotas');
const património_com_historia_url = património_route.concat('/património_com_historia');
const natureza_url = património_route.concat('/natureza');
const personalidades_url = património_route.concat('/personalidade');
const timeline_url = património_route.concat('/timeline');

export const património_options_routes = [
  {
    name: 'Agenda',
    type: 'agenda',
    url: agenda_url
  },
  {
    name: 'Rotas',
    type: 'route',
    url: rotas_url
  },
  {
    name: 'Património com Historia',
    type: 'material',
    url: património_com_historia_url
  },
  {
    name: 'Natureza',
    type: 'nature',
    url: natureza_url
  },
  {
    name: 'Personalidades',
    type: 'personality',
    url: personalidades_url
  },
  {
    name: 'Timeline',
    type: 'timeline',
    url: timeline_url
  }
];
