const all = {
  value: 'all',
  label: 'Todos'
};

const heritage = {
  value: 'heritage',
  label: 'Património Histórico'
};

const nature = {
  value: 'nature',
  label: 'Natureza'
};

const personality = {
  value: 'personality',
  label: 'Personalidades'
};

const route = {
  value: 'route',
  label: 'Rotas'
};

export const types = [all, heritage, nature, personality, route];

export const typesPath = [all, heritage, nature];
