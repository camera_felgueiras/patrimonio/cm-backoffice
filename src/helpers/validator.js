export const latValidation = (value, latHelper) => {
  let helper = latHelper;
  let error = false;
  let reg = /^-?([1-8]?[1-9]|[1-9]0).?[0-9]*$/;
  if (reg.test(value)) {
    if (!(isFinite(value) && Math.abs(value) <= 90)) {
      helper = 'Latitude Invalida';
      error = true;
    }
  } else {
    helper = 'Latitude Invalida';
    error = true;
  }
  return { value, error, helper };
};

export const lngValidation = (value, lngHelper) => {
  let helper = lngHelper;
  let error = false;
  let reg = /^-?([1-8]?[0-9]?[0-9]|[1-9]0).?[0-9]*$/;
  if (reg.test(value)) {
    if (!(isFinite(value) && Math.abs(value) <= 180)) {
      helper = 'Longitude Invalida';
      error = true;
    }
  } else {
    helper = 'Longitude Invalida';
    error = true;
  }
  return { value, error, helper };
};

export const descValidation = (value, descHelper) => {
  let helper = descHelper;
  let error = false;
  const reg = /.{3,8000}/gm;
  if (!reg.test(value)) {
    helper = `Descrição Invalida. Tamanho mínimo de 3 e máximo de 8000 caracteres atualmente ${value.length}`;
    error = true;
  }
  return { error, value, helper };
};

export const descSmallValidation = (value, descHelper) => {
  let helper = descHelper;
  let error = false;
  const reg = /.{3,500}/;
  if (!reg.test(value)) {
    helper = `Descrição Invalida. Tamanho mínimo de 3 e máximo de 500 caracteres atualmente ${value.length}`;
    error = true;
  }
  return { error, value, helper };
};

export const websiteValidation = (value, websiteHelper) => {
  let helper = websiteHelper;
  let error = false;
  const reg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  if (!reg.test(value)) {
    if (value.length > 0) {
      helper = 'Website invalido';
      error = true;
    }
  }
  return { error, value, helper };
};

export const phoneValidation = (value, phoneHelper) => {
  let helper = phoneHelper;
  let error = false;
  const reg = /^\d{9}$/;
  if (!reg.test(value)) {
    if (value.length > 0) {
      helper = 'Numero invalido, este deve ser 9 dígitos';
      error = true;
    }
  }
  return { error, value, helper };
};

export const emailValidation = (value, emailHelper) => {
  let helper = emailHelper;
  let error = false;
  const reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
  if (!reg.test(value)) {
    if (value.length > 0) {
      helper = 'Email invalido';
      error = true;
    }
  }
  return { error, value, helper };
};

export const addressValidation = (value, addressHelper) => {
  let helper = addressHelper;
  let error = false;
  if (value.length < 1) {
    helper = 'Morada Invalida';
    error = true;
  }
  return { error, value, helper };
};

export const kmValidation = (value, kmHelper) => {
  let helper = kmHelper;
  let error = false;
  const reg = /^[\d.]{1,}$/;
  if (!reg.test(value)) {
    if (value.length > 0) {
      helper = 'Km invalido';
      error = true;
    }
  }
  return { error, value, helper };
};

export const titleValidation = (value, nameHelper, errorHelper) => {
  let helper = nameHelper;
  let error = false;
  const reg = /^.{3,50}$/;
  if (!reg.test(value)) {
    helper = errorHelper.concat('. Tamanho mínimo de 3 e máximo de 50 caracteres');
    error = true;
  }
  return { value, error, helper };
};

export const jobValidation = (value, nameHelper, errorHelper) => {
  let helper = nameHelper;
  let error = false;
  const reg = /^.{3,200}$/;
  if (!reg.test(value)) {
    helper = errorHelper.concat('. Tamanho mínimo de 3 e máximo de 200 caracteres');
    error = true;
  }
  return { value, error, helper };
};

export const yearValidation = (value, helper, bigger) => {
  let error = false;
  const reg = /^[\d]{4}$/;
  if (!reg.test(value) || value < bigger) {
    helper = 'Ano Invalido';
    if (value < bigger) {
      helper = 'Ano têm de ser maior que ' + bigger;
    }
    error = true;
  }
  return { value, error, helper };
};

/*export const typeValidation = (value, typeHelper) => {
  let helper = typeHelper;
  let error = false;
  const trimmed = value.trim();
  if (trimmed.length < 1 || trimmed.length >= 30) {
    helper = 'Tipo invalido';
    error = true;
  }
  return { value, error, helper };
};*/
