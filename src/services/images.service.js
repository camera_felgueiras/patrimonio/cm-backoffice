import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const imageService = {
  uploadImage,
  deleteImage,
  getImage,
  getImageTh,
};

function uploadImage(formData) {
  const requestOptions = {
    method: 'POST',
    headers: { Authorization: getToken() },
    body: formData,
  };

  console.log(requestOptions);

  return fetch(`${apiUrl}/image`, requestOptions).then(handleResponse);
}

function deleteImage(image) {
  const requestOptions = {
    method: 'DELETE',
    headers: { Authorization: getToken() },
  };

  return fetch(`${apiUrl}/image/${image}`, requestOptions).then(handleResponse);
}

function getImage(name) {
  return `${apiUrl}/image/${name}`;
}

function getImageTh(name) {
  return `${apiUrl}/image/th/${name}`;
}
