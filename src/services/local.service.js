import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const localService = {
  getAll,
  getAllByType,
  getById,
  getImages,
  getRelated,
  create,
  update,
  delete: _delete,
};

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local`, requestOptions).then(handleResponse);
}

function getAllByType(type) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local?type=${type}`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local/${id}`, requestOptions).then(handleResponse);
}

function getImages(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local/images/${id}`, requestOptions).then(handleResponse);
}

function getRelated(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local/related/${id}`, requestOptions).then(handleResponse);
}

function update(id, local) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(local),
  };

  return fetch(`${apiUrl}/local/${id}`, requestOptions).then(handleResponse);
}

function create(local) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(local),
  };

  return fetch(`${apiUrl}/local`, requestOptions).then(handleResponse);
}

function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/local/${id}`, requestOptions).then(handleResponse);
}
