import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const personalityService = {
  getAll,
  getById,
  getImages,
  getRelated,
  create,
  update,
  delete: _delete,
};

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/personality`, requestOptions).then(handleResponse);
}

function create(personality) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(personality),
  };

  console.log(requestOptions.body);
  return fetch(`${apiUrl}/personality`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/personality/${id}`, requestOptions).then(handleResponse);
}

function getImages(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/personality/images/${id}`, requestOptions).then(handleResponse);
}

function getRelated(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/personality/related/${id}`, requestOptions).then(handleResponse);
}

function update(id, personality) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(personality),
  };

  console.log(requestOptions.body);

  return fetch(`${apiUrl}/personality/${id}`, requestOptions).then(handleResponse);
}

function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/personality/${id}`, requestOptions).then(handleResponse);
}
