import { apiQrCode } from '../helpers/configs';

export const qrCodeService = {
  get,
};

function get(data, size = 500) {
  const qr = {
    id: data.id,
    title: data.title || data.name,
    image: data.path,
    type: data.type,
  };

  return `${apiQrCode}size=${size}x${size}&data=${encodeURIComponent(JSON.stringify(qr))}`;
}
