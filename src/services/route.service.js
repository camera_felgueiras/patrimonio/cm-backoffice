import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const routeService = {
  getAll,
  create,
  getById,
  getImages,
  getRelated,
  getPath,
  update,
  delete: _delete,
};

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route/${id}`, requestOptions).then(handleResponse);
}

function getImages(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route/images/${id}`, requestOptions).then(handleResponse);
}

function getRelated(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route/related/${id}`, requestOptions).then(handleResponse);
}

function getPath(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route/path/${id}`, requestOptions).then(handleResponse);
}

function update(id, route) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(route),
  };

  return fetch(`${apiUrl}/route/${id}`, requestOptions).then(handleResponse);
}

function create(route) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(route),
  };

  return fetch(`${apiUrl}/route`, requestOptions).then(handleResponse);
}

function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/route/${id}`, requestOptions).then(handleResponse);
}
