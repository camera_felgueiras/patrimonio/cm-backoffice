import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const timelineService = {
  getAll,
  add,
  update,
  getById,
  getImage,
  getRelated,
  delete: _delete,
};

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/timeline`, requestOptions).then(handleResponse);
}

function add(fact) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(fact),
  };

  return fetch(`${apiUrl}/timeline`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/timeline/${id}`, requestOptions).then(handleResponse);
}

function getImage(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/timeline/image/${id}`, requestOptions).then(handleResponse);
}

function getRelated(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/timeline/related/${id}`, requestOptions).then(handleResponse);
}

function update(id, fact) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify(fact),
  };

  return fetch(`${apiUrl}/timeline/${id}`, requestOptions).then(handleResponse);
}

function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/timeline/${id}`, requestOptions).then(handleResponse);
}
