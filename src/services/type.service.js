import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const typeService = {
  getAll,
  createType,
  delete: _delete,
};

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/type`, requestOptions).then(handleResponse);
}

function createType(name) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify({ name }),
  };

  return fetch(`${apiUrl}/type`, requestOptions).then(handleResponse);
}

function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
  };

  return fetch(`${apiUrl}/type/${id}`, requestOptions).then(handleResponse);
}
