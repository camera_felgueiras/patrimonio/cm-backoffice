import { apiUrl, handleResponse } from '../helpers/configs';
import { getToken } from '../helpers/auth.helper.js';

export const userService = {
  login,
  logout,
  update,
  updatePassword,
};

function login(email, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password }),
  };

  return fetch(`${apiUrl}/user/auth`, requestOptions)
    .then(handleResponse)
    .then(data => {
      const user = data.user;
      localStorage.setItem('user', JSON.stringify(user));
      return user;
    });
}

function logout() {
  localStorage.removeItem('user');
}

function update({ id, firstName, lastName, email, password }) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify({ id, firstName, lastName, email, password }),
  };

  return fetch(`${apiUrl}/user`, requestOptions)
    .then(handleResponse)
    .then(data => {
      const user = data.user;
      localStorage.setItem('user', JSON.stringify(data.user));
      return user;
    });
}

function updatePassword({ id, password, newPassword }) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: getToken() },
    body: JSON.stringify({ id, password, newPassword }),
  };

  return fetch(`${apiUrl}/user/passwd`, requestOptions)
    .then(handleResponse)
    .then(data => {
      localStorage.removeItem('user');
      return 'OK';
    });
}
