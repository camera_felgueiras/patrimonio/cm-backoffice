import { categoriesConstants } from '../constants/categories.constants';
import { categoryService } from '../../services/category.service';
import { alertActions } from './alert.actions';

export const categoriesActions = {
  getAll,
  create,
  getByType,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    categoryService
      .getAll()
      .then(data => {
        dispatch(success(data.categories));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: categoriesConstants.GET_ALL_REQUEST };
  }
  function success(list) {
    return { type: categoriesConstants.GET_ALL_SUCCESS, list };
  }
  function failure(error) {
    return { type: categoriesConstants.GET_ALL_FAILURE, error };
  }
}

function getByType(idType) {
  return dispatch => {
    dispatch(request());

    categoryService
      .getByType(idType)
      .then(data => {
        dispatch(success(data.categories));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: categoriesConstants.GET_BY_TYPE_REQUEST };
  }
  function success(list) {
    return { type: categoriesConstants.GET_BY_TYPE_SUCCESS, list };
  }
  function failure() {
    return { type: categoriesConstants.GET_BY_TYPE_FAILURE };
  }
}

function create(name, type) {
  return dispatch => {
    dispatch(request());

    categoryService
      .create({ name, type })
      .then(data => {
        dispatch(success(data));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: categoriesConstants.CREATE_REQUEST };
  }
  function success(category) {
    return { type: categoriesConstants.CREATE_SUCCESS, category };
  }
  function failure(error) {
    return { type: categoriesConstants.CREATE_FAILURE, error };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request());

    categoryService
      .delete(id)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString));
      });
  };

  function request() {
    return { type: categoriesConstants.DELETE_REQUEST };
  }
  function success() {
    return { type: categoriesConstants.DELETE_SUCCESS };
  }
  function failure() {
    return { type: categoriesConstants.DELETE_FAILURE };
  }
}
