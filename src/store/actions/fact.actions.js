import { factConstants } from '../constants/fact.constants';

export const factActions = {
  get,
  add,
  delete: _delete,
  clear
};

function get() {
  return dispatch => {
    dispatch({ type: factConstants.GET_ALL });
  };
}

function add(fact) {
  return dispatch => {
    dispatch({ type: factConstants.ADD_FACT, fact });
  };
}

function _delete(fact) {
  return dispatch => {
    dispatch({ type: factConstants.DELETE_FACT, fact });
  };
}

function clear() {
  return dispatch => {
    dispatch({ type: factConstants.CLEAR_FACTS });
  };
}
