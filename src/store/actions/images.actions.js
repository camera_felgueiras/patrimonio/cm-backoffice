import { imagesConstant } from '../constants/images.constants';
import { alertActions } from './alert.actions';
import { imageService } from '../../services/images.service';

export const imageActions = {
  clear,
  upload,
  delete: _delete
};

function clear() {
  return dispatch => {
    dispatch({ type: imagesConstant.CLEAR_IMAGES });
  };
}

function upload(type, formData) {
  return dispatch => {
    dispatch(request());

    imageService
      .uploadImage(formData)
      .then(images => {
        dispatch(success(images, type));
      })
      .catch(error => {
        let message = 'Erro ao carregar imagens';
        if (error.toString() === 'File to large') {
          message = 'Ficheiro muito grande';
        } else if (error.toString() === 'Unexpected field') {
          message = 'Muitos Ficheiros Maximum 8';
        }
        dispatch(alertActions.error(message));
        dispatch(failure());
      });
  };

  function request() {
    return { type: imagesConstant.UPLOAD_REQUEST };
  }
  function success(list, imageType) {
    return { type: imagesConstant.UPLOAD_SUCCESS, list, imageType };
  }
  function failure() {
    return { type: imagesConstant.UPLOAD_FAILURE };
  }
}

function _delete(isList, image, edit, changed) {
  return dispatch => {
    dispatch(request());

    if (!edit || isList) {
      imageService
        .deleteImage(image)
        .then(data => {
          dispatch(success({ isList, filename: data.filename }));
          dispatch(alertActions.success(data.message));
        })
        .catch(error => {
          dispatch(alertActions.error(error.toString()));
          dispatch(failure(error.toString()));
        });
    } else {
      dispatch(success({ isList, filename: image, changed: true }));
    }
  };

  function request() {
    return { type: imagesConstant.DELETE_REQUEST };
  }
  function success(options) {
    return { type: imagesConstant.DELETE_SUCCESS, options };
  }
  function failure() {
    return { type: imagesConstant.DELETE_FAILURE };
  }
}
