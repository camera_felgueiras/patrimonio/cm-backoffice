import { localConstants } from '../constants/local.constants';
import { imagesConstant } from '../constants/images.constants';
import { relatedConstants } from '../constants/related.constants';
import { alertActions } from './alert.actions';
import { localService } from '../../services/local.service';

export const localActions = {
  clear,
  getAll,
  filter,
  getAllByType,
  getById,
  getImages,
  getRelated,
  create,
  update,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    localService
      .getAll()
      .then(data => {
        dispatch(success(data.locals));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.GET_ALL_REQUEST };
  }
  function success(list) {
    return { type: localConstants.GET_ALL_SUCCESS, list };
  }
  function failure(error) {
    return { type: localConstants.GET_ALL_FAILURE, error };
  }
}

function getAllByType(type) {
  return dispatch => {
    dispatch(request());

    localService
      .getAllByType(type)
      .then(data => {
        dispatch(success(data.locals));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.GET_ALL_BY_TYPE_REQUEST };
  }
  function success(list) {
    return { type: localConstants.GET_ALL_BY_TYPE_SUCCESS, list };
  }
  function failure(error) {
    return { type: localConstants.GET_ALL_BY_TYPE_FAILURE, error };
  }
}

function filter(query) {
  return dispatch => {
    dispatch({ type: localConstants.FILTER, query });
  };
}

function getById(id) {
  return dispatch => {
    dispatch(request());

    localService
      .getById(id)
      .then(data => {
        dispatch(success(data.local));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.GET_BY_ID_REQUEST };
  }
  function success(local) {
    return { type: localConstants.GET_BY_ID_SUCCESS, local };
  }
  function failure() {
    return { type: localConstants.GET_BY_ID_FAILURE };
  }
}

function getImages(id) {
  return dispatch => {
    dispatch(request());

    localService
      .getImages(id)
      .then(data => {
        dispatch(success(data.images, data.mainImage));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString));
      });
  };

  function request() {
    return { type: imagesConstant.GET_BY_ID_REQUEST };
  }
  function success(list, mainImage) {
    return { type: imagesConstant.GET_BY_ID_SUCCESS, list, mainImage };
  }
  function failure() {
    return { type: imagesConstant.GET_BY_ID_FAILURE };
  }
}

function getRelated(id) {
  return dispatch => {
    dispatch(request());

    localService
      .getRelated(id)
      .then(data => {
        const res = data.related;
        const p = res.personalities;
        const l = res.locals;
        const r = res.routes;
        const related = p.concat(l, r);
        dispatch(success(related));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: relatedConstants.GET_ALL_REQUEST };
  }
  function success(related) {
    return { type: relatedConstants.GET_ALL_SUCCESS, related };
  }
  function failure() {
    return { type: relatedConstants.GET_ALL_FAILURE };
  }
}

function update(id, local) {
  return dispatch => {
    dispatch(request());

    localService
      .update(id, local)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: localConstants.UPDATE_SUCCESS };
  }
  function failure(error) {
    return { type: localConstants.UPDATE_FAILURE, error };
  }
}

function create(local) {
  return dispatch => {
    dispatch(request());

    localService
      .create(local)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.CREATE_REQUEST };
  }
  function success() {
    return { type: localConstants.CREATE_SUCCESS };
  }
  function failure(error) {
    return { type: localConstants.CREATE_FAILURE, error };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request());

    localService
      .delete(id)
      .then(data => {
        dispatch(success(id));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: localConstants.DELETE_REQUEST };
  }
  function success(id) {
    return { type: localConstants.DELETE_SUCCESS, id };
  }
  function failure(error) {
    return { type: localConstants.DELETE_FAILURE, error };
  }
}

function clear() {
  return { type: localConstants.CLEAR };
}
