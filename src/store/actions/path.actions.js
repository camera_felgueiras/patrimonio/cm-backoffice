import { pathConstants } from '../constants/path.constants';

export const pathActions = {
  get,
  add,
  delete: _delete,
  clear,
};

function get() {
  return dispatch => {
    dispatch({ type: pathConstants.GET_ALL });
  };
}

function add(path) {
  return dispatch => {
    dispatch({ type: pathConstants.ADD_PATH, path });
  };
}

function _delete(path) {
  return dispatch => {
    dispatch({ type: pathConstants.DELETE_PATH, path });
  };
}

function clear() {
  return dispatch => {
    dispatch({ type: pathConstants.CLEAR_PATH });
  };
}
