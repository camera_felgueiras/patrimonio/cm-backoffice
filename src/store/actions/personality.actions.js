import { personalityConstants } from '../constants/personality.constants';
import { imagesConstant } from '../constants/images.constants';
import { relatedConstants } from '../constants/related.constants';
import { alertActions } from './alert.actions';
import { personalityService } from '../../services/personality.service';

export const personalityActions = {
  getAll,
  filter,
  getById,
  getImages,
  getRelated,
  create,
  update,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    personalityService
      .getAll()
      .then(data => {
        dispatch(success(data.personalities));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: personalityConstants.GET_ALL_REQUEST };
  }
  function success(list) {
    return { type: personalityConstants.GET_ALL_SUCCESS, list };
  }
  function failure(error) {
    return { type: personalityConstants.GET_ALL_FAILURE, error };
  }
}

function filter(query) {
  return dispatch => {
    dispatch({ type: personalityConstants.FILTER, query });
  };
}

function getById(id) {
  return dispatch => {
    dispatch(request());

    personalityService
      .getById(id)
      .then(data => {
        dispatch(success(data.personality));
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: personalityConstants.GET_BY_ID_REQUEST };
  }
  function success(personality) {
    return { type: personalityConstants.GET_BY_ID_SUCCESS, personality };
  }
  function failure() {
    return { type: personalityConstants.GET_BY_ID_FAILURE };
  }
}

function getImages(id) {
  return dispatch => {
    dispatch(request());

    personalityService
      .getImages(id)
      .then(data => {
        dispatch(success(data.images, data.mainImage));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString));
      });
  };

  function request() {
    return { type: imagesConstant.GET_BY_ID_REQUEST };
  }
  function success(list, mainImage) {
    return { type: imagesConstant.GET_BY_ID_SUCCESS, list, mainImage };
  }
  function failure() {
    return { type: imagesConstant.GET_BY_ID_FAILURE };
  }
}

function getRelated(id) {
  return dispatch => {
    dispatch(request());

    personalityService
      .getRelated(id)
      .then(data => {
        const res = data.related;
        const p = res.personalities;
        const l = res.locals;
        const r = res.routes;
        const related = p.concat(l, r);
        dispatch(success(related));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: relatedConstants.GET_ALL_REQUEST };
  }
  function success(related) {
    return { type: relatedConstants.GET_ALL_SUCCESS, related };
  }
  function failure() {
    return { type: relatedConstants.GET_ALL_FAILURE };
  }
}

function create(personality) {
  return dispatch => {
    dispatch(request());

    personalityService
      .create(personality)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: personalityConstants.CREATE_REQUEST };
  }
  function success() {
    return { type: personalityConstants.CREATE_SUCCESS };
  }
  function failure(error) {
    return { type: personalityConstants.CREATE_FAILURE, error };
  }
}

function update(id, personality) {
  return dispatch => {
    dispatch(request());

    personalityService
      .update(id, personality)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: personalityConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: personalityConstants.UPDATE_SUCCESS };
  }
  function failure() {
    return { type: personalityConstants.UPDATE_FAILURE };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request());

    personalityService
      .delete(id)
      .then(data => {
        dispatch(success(id));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: personalityConstants.DELETE_REQUEST };
  }
  function success(id) {
    return { type: personalityConstants.DELETE_SUCCESS, id };
  }
  function failure(error) {
    return { type: personalityConstants.DELETE_FAILURE, error };
  }
}
