import { relatedConstants } from '../constants/related.constants';

export const relatedActions = {
  get,
  add,
  delete: _delete,
  clear
};

function get() {
  return dispatch => {
    dispatch({ type: relatedConstants.GET_ALL });
  };
}

function add(related) {
  return dispatch => {
    dispatch({ type: relatedConstants.ADD_RELATED, related });
  };
}

function _delete(related) {
  return dispatch => {
    dispatch({ type: relatedConstants.DELETE_RELATED, related });
  };
}

function clear() {
  return dispatch => {
    dispatch({ type: relatedConstants.CLEAR_RELATED });
  };
}
