import { routeConstants } from '../constants/route.constants';
import { imagesConstant } from '../constants/images.constants';
import { relatedConstants } from '../constants/related.constants';
import { pathConstants } from '../constants/path.constants';
import { alertActions } from './alert.actions';
import { routeService } from '../../services/route.service';

export const routeActions = {
  getAll,
  filter,
  getAllByType,
  getById,
  getImages,
  getRelated,
  getPath,
  create,
  update,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    routeService
      .getAll()
      .then(data => {
        dispatch(success(data.routes));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.GET_ALL_REQUEST };
  }
  function success(list) {
    return { type: routeConstants.GET_ALL_SUCCESS, list };
  }
  function failure(error) {
    return { type: routeConstants.GET_ALL_FAILURE, error };
  }
}

function getAllByType(type) {
  return dispatch => {
    dispatch(request());

    routeService
      .getAllByType(type)
      .then(data => {
        dispatch(success(data.routes));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.GET_ALL_BY_TYPE_REQUEST };
  }
  function success(list) {
    return { type: routeConstants.GET_ALL_BY_TYPE_SUCCESS, list };
  }
  function failure(error) {
    return { type: routeConstants.GET_ALL_BY_TYPE_FAILURE, error };
  }
}

function filter(query) {
  return dispatch => {
    dispatch({ type: routeConstants.FILTER, query });
  };
}

function getById(id) {
  return dispatch => {
    dispatch(request());

    routeService
      .getById(id)
      .then(data => {
        dispatch(success(data.route));
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.GET_BY_ID_REQUEST };
  }
  function success(route) {
    return { type: routeConstants.GET_BY_ID_SUCCESS, route };
  }
  function failure() {
    return { type: routeConstants.GET_BY_ID_FAILURE };
  }
}

function getImages(id) {
  return dispatch => {
    dispatch(request());

    routeService
      .getImages(id)
      .then(data => {
        dispatch(success(data.images, data.mainImage));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString));
      });
  };

  function request() {
    return { type: imagesConstant.GET_BY_ID_REQUEST };
  }
  function success(list, mainImage) {
    return { type: imagesConstant.GET_BY_ID_SUCCESS, list, mainImage };
  }
  function failure() {
    return { type: imagesConstant.GET_BY_ID_FAILURE };
  }
}

function getRelated(id) {
  return dispatch => {
    dispatch(request());

    routeService
      .getRelated(id)
      .then(data => {
        const res = data.related;
        const p = res.personalities;
        const l = res.locals;
        const r = res.routes;
        const related = p.concat(l, r);
        dispatch(success(related));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: relatedConstants.GET_ALL_REQUEST };
  }
  function success(related) {
    return { type: relatedConstants.GET_ALL_SUCCESS, related };
  }
  function failure() {
    return { type: relatedConstants.GET_ALL_FAILURE };
  }
}

function getPath(id) {
  return dispatch => {
    dispatch(request());

    routeService
      .getPath(id)
      .then(data => {
        dispatch(success(data.path));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: pathConstants.GET_ALL_REQUEST };
  }
  function success(path) {
    return { type: pathConstants.GET_ALL_SUCCESS, path };
  }
  function failure() {
    return { type: pathConstants.GET_ALL_FAILURE };
  }
}

function update(id, route) {
  return dispatch => {
    dispatch(request());

    routeService
      .update(id, route)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: routeConstants.UPDATE_SUCCESS };
  }
  function failure(error) {
    return { type: routeConstants.UPDATE_FAILURE, error };
  }
}

function create(route) {
  return dispatch => {
    dispatch(request());

    routeService
      .create(route)
      .then(data => {
        dispatch(success());
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.CREATE_REQUEST };
  }
  function success() {
    return { type: routeConstants.CREATE_SUCCESS };
  }
  function failure(error) {
    return { type: routeConstants.CREATE_FAILURE, error };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request());

    routeService
      .delete(id)
      .then(data => {
        dispatch(success(id));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: routeConstants.DELETE_REQUEST };
  }
  function success(id) {
    return { type: routeConstants.DELETE_SUCCESS, id };
  }
  function failure(error) {
    return { type: routeConstants.DELETE_FAILURE, error };
  }
}
