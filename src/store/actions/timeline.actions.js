import { timelineConstants } from '../constants/timeline.constants';
import { imagesConstant } from '../constants/images.constants';
import { relatedConstants } from '../constants/related.constants';
import { alertActions } from './alert.actions';

import { timelineService } from '../../services/timeline.service';

export const timelineActions = {
  getAll,
  getById,
  getImage,
  getRelated,
  add,
  update,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    timelineService
      .getAll()
      .then(data => {
        dispatch(success(data.timeline));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: timelineConstants.GET_ALL_REQUEST };
  }
  function success(list) {
    return { type: timelineConstants.GET_ALL_SUCCESS, list };
  }
  function failure() {
    return { type: timelineConstants.GET_ALL_FAILURE };
  }
}

function getById(id) {
  return dispatch => {
    dispatch(request());

    timelineService
      .getById(id)
      .then(data => {
        dispatch(success(data.timeline));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: timelineConstants.GET_BY_ID_REQUEST };
  }
  function success(fact) {
    return { type: timelineConstants.GET_BY_ID_SUCCESS, fact };
  }
  function failure() {
    return { type: timelineConstants.GET_BY_ID_FAILURE };
  }
}

function getImage(id) {
  return dispatch => {
    dispatch(request());

    timelineService
      .getImage(id)
      .then(data => dispatch(success(data.images)))
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: imagesConstant.GET_BY_ID_REQUEST };
  }
  function success(list) {
    return { type: imagesConstant.GET_BY_ID_SUCCESS, list, imageType: 'single' };
  }
  function failure() {
    return { type: imagesConstant.GET_BY_ID_FAILURE };
  }
}

function getRelated(id) {
  return dispatch => {
    dispatch(request());

    timelineService
      .getRelated(id)
      .then(data => dispatch(success(data.related)))
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: relatedConstants.GET_ALL_REQUEST };
  }
  function success(related) {
    return { type: relatedConstants.GET_ALL_SUCCESS, related };
  }
  function failure() {
    return { type: relatedConstants.GET_ALL_FAILURE };
  }
}

function update(id, fact) {
  return dispatch => {
    dispatch(request());

    timelineService
      .update(id, fact)
      .then(data => {
        dispatch(success(data));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: timelineConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: timelineConstants.UPDATE_SUCCESS };
  }
  function failure() {
    return { type: timelineConstants.UPDATE_FAILURE };
  }
}

function add(fact) {
  return dispatch => {
    dispatch(request());

    timelineService
      .add(fact)
      .then(data => {
        dispatch(success(data.timeline));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: timelineConstants.CREATE_REQUEST };
  }
  function success(list) {
    return { type: timelineConstants.CREATE_SUCCESS, list };
  }
  function failure() {
    return { type: timelineConstants.CREATE_FAILURE };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request());

    timelineService
      .delete(id)
      .then(data => {
        dispatch(success(id));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure());
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: timelineConstants.DELETE_REQUEST };
  }
  function success(id) {
    return { type: timelineConstants.DELETE_SUCCESS, id };
  }
  function failure() {
    return { type: timelineConstants.DELETE_FAILURE };
  }
}
