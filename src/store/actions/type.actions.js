import { typeConstants } from '../constants/type.constants';
import { alertActions } from './alert.actions';

import { typeService } from '../../services/type.service';

export const typeActions = {
  getAll,
  createType,
  delete: _delete
};

function getAll() {
  return dispatch => {
    dispatch(request());

    typeService
      .getAll()
      .then(data => {
        dispatch(success(data.types));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: typeConstants.GET_ALL_REQUEST };
  }
  function success(listTypes) {
    return { type: typeConstants.GET_ALL_SUCCESS, listTypes };
  }
  function failure(error) {
    return { type: typeConstants.GET_ALL_FAILURE, error };
  }
}

function createType(name) {
  return dispatch => {
    dispatch(request());

    typeService
      .createType(name)
      .then(data => {
        dispatch(success(data.type));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request() {
    return { type: typeConstants.CREATE_REQUEST };
  }
  function success(typeInfo) {
    return { type: typeConstants.CREATE_SUCCESS, typeInfo };
  }
  function failure(error) {
    return { type: typeConstants.CREATE_FAILURE, error };
  }
}

function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    typeService
      .delete(id)
      .then(data => {
        dispatch(success(data.message));
        dispatch(alertActions.success(data.message));
      })
      .catch(error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request(id) {
    return { type: typeConstants.DELETE_REQUEST, id };
  }
  function success(message) {
    return { type: typeConstants.DELETE_SUCCESS, message };
  }
  function failure(error) {
    return { type: typeConstants.DELETE_FAILURE, error };
  }
}
