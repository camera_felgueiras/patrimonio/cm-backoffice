import { userConstants } from '../constants/user.constants';
import { userService } from '../../services/user.service';
import { alertActions } from './alert.actions';
import { history } from '../../helpers/history';

export const userActions = {
  login,
  logout,
  update,
  password
};

function login(email, password) {
  return dispatch => {
    dispatch(request({ email }));

    userService.login(email, password).then(
      user => {
        dispatch(success(user));
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function update(prev, user) {
  return dispatch => {
    dispatch(request(prev));

    userService.update(user).then(
      user => {
        dispatch(success(user));
      },
      error => {
        dispatch(failure(error, prev));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request(user) {
    return { type: userConstants.UPDATE_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.UPDATE_SUCCESS, user };
  }
  function failure(error, user) {
    return { type: userConstants.UPDATE_FAILURE, error, user };
  }
}

function password(user) {
  return dispatch => {
    userService.updatePassword(user).then(
      data => {
        if (data === 'OK') {
          history.push('/');
        }
      },
      error => {
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
