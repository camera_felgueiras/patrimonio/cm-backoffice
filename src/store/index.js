import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import { alert } from './reducers/alert.reducer';
import { authentication } from './reducers/auth.reducer';
import { users } from './reducers/user.reducer';
import { type } from './reducers/type.reducer';
import { category } from './reducers/categories.reducer';
import { images } from './reducers/image.reducer';
import { facts } from './reducers/fact.reducer';
import { personalities } from './reducers/personality.reducer';
import { locals } from './reducers/local.reducer';
import { related } from './reducers/related.reducer';
import { path } from './reducers/path.reducer';
import { routes } from './reducers/route.reducer';
import { timeline } from './reducers/timeline.reducer';

const allReducers = combineReducers({
  alert,
  authentication,
  users,
  type,
  category,
  images,
  facts,
  personalities,
  locals,
  related,
  path,
  routes,
  timeline
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const allStoreEnhancers = composeEnhancers(applyMiddleware(thunk));

const store = createStore(allReducers, allStoreEnhancers);

export default store;
