import { alertConstants } from '../constants/alert.constants';

export const alert = (state = {}, { type, message }) => {
  switch (type) {
    case alertConstants.SUCCESS:
      return {
        type: 'success',
        message
      };
    case alertConstants.ERROR:
      return {
        type: 'error',
        message
      };
    case alertConstants.INFO:
      return {
        type: 'info',
        message
      };
    case alertConstants.CLEAR:
      return {};
    default:
      return state;
  }
};
