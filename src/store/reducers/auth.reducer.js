import { userConstants } from '../constants/user.constants';

let user = localStorage ? JSON.parse(localStorage.getItem('user')) : undefined;
const initialState = user ? { loggedIn: true, user } : {};

export const authentication = (state = initialState, action) => {
  switch (action.type) {
    case userConstants.UPDATE_REQUEST:
      return {
        loading: true,
        loggedIn: true,
        user: action.user
      };
    case userConstants.UPDATE_SUCCESS:
      return {
        loading: false,
        loggedIn: true,
        user: action.user
      };
    case userConstants.UPDATE_FAILURE:
      return {
        loading: false,
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_REQUEST:
      return {
        loading: true,
        loggedIn: false,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loading: false,
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {
        user: action.user
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
};
