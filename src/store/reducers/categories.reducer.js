import { categoriesConstants } from '../constants/categories.constants';

const defaultCategory = { loading: false, listCategories: [] };

export const category = (state = defaultCategory, action) => {
  switch (action.type) {
    case categoriesConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true,
        listCategories: [],
        updateList: false
      };
    case categoriesConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        loading: false,
        listCategories: action.list,
        updateList: false
      };
    case categoriesConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false,
        listCategories: [],
        updateList: false
      };
    case categoriesConstants.GET_BY_TYPE_REQUEST:
      return {
        ...state,
        loading: true,
        listCategories: [],
        updateList: false
      };
    case categoriesConstants.GET_BY_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        listCategories: action.list,
        updateList: false
      };
    case categoriesConstants.GET_BY_TYPE_FAILURE:
      return {
        ...state,
        loading: false,
        listCategories: [],
        updateList: false
      };
    case categoriesConstants.CREATE_REQUEST:
      return {
        ...state,
        loading: true,
        updateList: false
      };
    case categoriesConstants.CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateList: true
      };
    case categoriesConstants.CREATE_FAILURE:
      return {
        ...state,
        loading: false,
        updateList: false
      };
    case categoriesConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case categoriesConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateList: true
      };
    case categoriesConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false,
        updateList: false
      };
    default:
      return state;
  }
};
