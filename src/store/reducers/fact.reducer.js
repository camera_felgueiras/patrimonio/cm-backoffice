import { factConstants } from '../constants/fact.constants';

const initialState = { facts: [], fact: {}, id: 0 };

export const facts = (state = initialState, { type, fact }) => {
  switch (type) {
    case factConstants.ADD_FACT:
      const id = state.id + 1;
      fact.id = id;
      return {
        fact,
        facts: [...state.facts, fact],
        id
      };
    case factConstants.DELETE_FACT:
      return {
        fact,
        facts: state.facts.filter(f => f !== fact),
        id: state.id
      };
    case factConstants.CLEAR_FACTS:
      return initialState;
    case factConstants.GET_ALL:
      return state;
    default:
      return state;
  }
};
