import { imagesConstant } from '../constants/images.constants';

const initialState = { loading: false, images: [], mainImage: null, changed: false };

export const images = (state = initialState, action) => {
  switch (action.type) {
    case imagesConstant.UPLOAD_REQUEST:
      return {
        ...state,
        loading: true
      };
    case imagesConstant.UPLOAD_SUCCESS:
      if (action.imageType === 'single') {
        return {
          ...state,
          loading: false,
          type: action.imageType,
          mainImage: action.list[0]
        };
      }
      return {
        ...state,
        loading: false,
        type: action.imageType,
        images: state.images.concat(action.list)
      };
    case imagesConstant.UPLOAD_FAILURE:
      return {
        ...state,
        loading: false
      };
    case imagesConstant.DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case imagesConstant.DELETE_SUCCESS:
      const { isList, filename } = action.options;
      if (isList) {
        return {
          ...state,
          images: state.images.filter(item => item.url !== filename),
          loading: false
        };
      }
      return {
        ...state,
        loading: false,
        mainImage: null
      };
    case imagesConstant.DELETE_FAILURE:
      return {
        ...state,
        loading: false
      };
    case imagesConstant.GET_BY_ID_REQUEST:
      return {
        ...state,
        loading: true
      };
    case imagesConstant.GET_BY_ID_SUCCESS:
      if (action.imageType === 'single') {
        return {
          ...state,
          loading: false,
          type: action.imageType,
          mainImage: action.list[0]
        };
      }
      const image = action.mainImage;
      image.url = action.mainImage.path;
      return {
        ...state,
        loading: false,
        type: action.imageType,
        mainImage: image || state.mainImage,
        images: action.list.map(v => {
          v.url = v.path;
          return v;
        })
      };
    case imagesConstant.GET_BY_ID_FAILURE:
      return {
        ...state,
        loading: false
      };
    case imagesConstant.CLEAR_IMAGES:
      return initialState;
    default:
      return state;
  }
};
