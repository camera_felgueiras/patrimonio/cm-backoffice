import { localConstants } from '../constants/local.constants';

const initialState = { loading: false, locals: [], local: {} };

export const locals = (state = initialState, action) => {
  switch (action.type) {
    case localConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case localConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        loading: false,
        locals: action.list
      };
    case localConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false
      };
    case localConstants.FILTER:
      let localsFiltered;
      if (action.query !== '') {
        localsFiltered = state.locals.filter(val => {
          return val.title.toLowerCase().indexOf(action.query) !== -1;
        });
      } else {
        localsFiltered = state.locals;
      }
      return {
        ...state,
        localsFiltered
      };
    case localConstants.GET_ALL_BY_TYPE_REQUEST:
      return {
        ...state,
        loading: true,
        locals: []
      };
    case localConstants.GET_ALL_BY_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        locals: action.list
      };
    case localConstants.GET_ALL_BY_TYPE_FAILURE:
      return {
        ...state,
        loading: false,
        locals: []
      };
    case localConstants.GET_BY_ID_REQUEST:
      return {
        ...state,
        loading: true
      };
    case localConstants.GET_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        local: action.local
      };
    case localConstants.GET_BY_ID_FAILURE:
      return {
        ...state,
        loading: false
      };
    case localConstants.CREATE_REQUEST:
      return state;
    case localConstants.CREATE_SUCCESS:
      return state;
    case localConstants.CREATE_FAILURE:
      return state;
    case localConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case localConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        locals: state.locals.filter(p => p.id !== action.id)
      };
    case localConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false
      };
    case localConstants.CLEAR:
      return initialState;
    default:
      return state;
  }
};
