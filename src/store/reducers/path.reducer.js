import { pathConstants } from '../constants/path.constants';

const initialState = { path: [], id: 0 };

export const path = (state = initialState, { type, path }) => {
  switch (type) {
    case pathConstants.ADD_PATH:
      const id = state.id + 1;
      path.p_id = id;
      return {
        path: [...state.path, path],
        id,
      };
    case pathConstants.DELETE_PATH:
      return {
        path: state.path.filter(f => f.id !== path),
        id: state.id,
      };
    case pathConstants.CLEAR_PATH:
      return initialState;
    case pathConstants.GET_ALL:
      return state;
    case pathConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case pathConstants.GET_ALL_SUCCESS:
      let localType = path;
      localType.map(l => {
        if (l.type === 'Natureza') l.type = 'nature';
        else if (l.type === 'Património Histórico') l.type = 'heritage';
        return l;
      });
      return {
        ...state,
        loading: false,
        path: localType,
      };
    case pathConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
