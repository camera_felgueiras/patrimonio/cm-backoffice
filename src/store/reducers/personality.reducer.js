import { personalityConstants } from '../constants/personality.constants';

const initialState = { loading: false, personalities: [], personality: {} };

export const personalities = (state = initialState, action) => {
  switch (action.type) {
    case personalityConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case personalityConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        loading: false,
        personalities: action.list,
      };
    case personalityConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case personalityConstants.FILTER:
      let personalitiesFiltered;
      if (action.query !== '') {
        personalitiesFiltered = state.personalities.filter(val => {
          return val.name.toLowerCase().indexOf(action.query) !== -1;
        });
      } else {
        personalitiesFiltered = state.personalities;
      }
      return {
        ...state,
        personalitiesFiltered,
      };
    case personalityConstants.GET_BY_ID_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case personalityConstants.GET_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        personality: action.personality,
      };
    case personalityConstants.GET_BY_ID_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case personalityConstants.CREATE_REQUEST:
      return state;
    case personalityConstants.CREATE_SUCCESS:
      return state;
    case personalityConstants.CREATE_FAILURE:
      return state;
    case personalityConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case personalityConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        personalities: state.personalities.filter(p => p.id !== action.id),
      };
    case personalityConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
