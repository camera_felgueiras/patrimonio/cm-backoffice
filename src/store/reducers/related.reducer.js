import { relatedConstants } from '../constants/related.constants';

const initialState = { relations: [], related: {}, id: 0, loading: false };

export const related = (state = initialState, { type, related }) => {
  switch (type) {
    case relatedConstants.ADD_RELATED:
      const id = state.id + 1;
      related.related_id = id;
      return {
        ...state,
        related,
        relations: [...state.relations, related],
        id
      };
    case relatedConstants.DELETE_RELATED:
      return {
        ...state,
        relations: state.relations.filter(f => f.id === related.id),
        id: state.id
      };
    case relatedConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case relatedConstants.GET_ALL_SUCCESS:
      related.map(r => {
        if (r.type === 'Natureza') r.type = 'nature';
        else if (r.type === 'Património Histórico') r.type = 'heritage';
        else if (!r.type) r.type = 'personality';
        return r;
      });
      return {
        ...state,
        loading: false,
        relations: [...related]
      };
    case relatedConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false
      };
    case relatedConstants.CLEAR_RELATED:
      return initialState;
    case relatedConstants.GET_ALL:
      return state;
    default:
      return state;
  }
};
