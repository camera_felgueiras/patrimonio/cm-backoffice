import { routeConstants } from '../constants/route.constants';

const initialState = { loading: false, routes: [], route: {} };

export const routes = (state = initialState, action) => {
  switch (action.type) {
    case routeConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case routeConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        loading: false,
        routes: action.list
      };
    case routeConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false
      };
    case routeConstants.FILTER:
      let routesFiltered;
      if (action.query !== '') {
        routesFiltered = state.routes.filter(val => {
          return val.title.toLowerCase().indexOf(action.query) !== -1;
        });
      } else {
        routesFiltered = state.routes;
      }
      return {
        ...state,
        routesFiltered
      };
    case routeConstants.GET_ALL_BY_TYPE_REQUEST:
      return {
        ...state,
        loading: true,
        routes: []
      };
    case routeConstants.GET_ALL_BY_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        routes: action.list
      };
    case routeConstants.GET_ALL_BY_TYPE_FAILURE:
      return {
        ...state,
        loading: false,
        routes: []
      };
    case routeConstants.GET_BY_ID_REQUEST:
      return {
        ...state,
        loading: true
      };
    case routeConstants.GET_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        route: action.route
      };
    case routeConstants.GET_BY_ID_FAILURE:
      return {
        ...state,
        loading: false
      };
    case routeConstants.CREATE_REQUEST:
      return state;
    case routeConstants.CREATE_SUCCESS:
      return state;
    case routeConstants.CREATE_FAILURE:
      return state;
    case routeConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case routeConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        routes: state.routes.filter(p => p.id !== action.id)
      };
    case routeConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
};
