import { timelineConstants } from '../constants/timeline.constants';

const initState = { timeline: [], loading: false };

export const timeline = (state = initState, action) => {
  switch (action.type) {
    case timelineConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case timelineConstants.GET_ALL_SUCCESS:
      const list = action.list;
      let t = [];
      if (list.personalities) {
        t = [...list.personalities, ...list.locals];
      }

      return {
        ...state,
        loading: false,
        timeline: t
      };
    case timelineConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false
      };
    case timelineConstants.GET_BY_ID_REQUEST:
      return {
        ...state,
        loading: true
      };
    case timelineConstants.GET_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        fact: action.fact
      };
    case timelineConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };
    case timelineConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        timeline: state.timeline.filter(t => t.id !== action.id)
      };
    case timelineConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
};
