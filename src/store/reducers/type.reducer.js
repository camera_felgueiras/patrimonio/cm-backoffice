import { typeConstants } from '../constants/type.constants';

const defaultTypeState = { loading: false, firstVal: -1, listTypes: [] };

export const type = (state = defaultTypeState, action) => {
  const type = action.type;
  switch (type) {
    case typeConstants.GET_ALL_REQUEST:
      return {
        ...state,
        loading: true,
        updateList: false
      };
    case typeConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        loading: false,
        firstVal: action.listTypes[0].id,
        listTypes: action.listTypes,
        updateList: false
      };
    case typeConstants.GET_ALL_FAILURE:
      return {
        ...state,
        loading: false,
        firstVal: state.listTypes[0].id,
        listTypes: state.listTypes,
        updateList: false
      };
    case typeConstants.CREATE_REQUEST:
      return {
        ...state,
        loading: true,
        updateList: false
      };
    case typeConstants.CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateList: true
      };
    case typeConstants.CREATE_FAILURE:
      return {
        ...state,
        loading: false,
        updateList: false
      };
    case typeConstants.DELETE_REQUEST:
      return {
        ...state,
        loading: true,
        updateList: false
      };
    case typeConstants.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateList: true
      };
    case typeConstants.DELETE_FAILURE:
      return {
        ...state,
        loading: false,
        updateList: false
      };
    default:
      return state;
  }
};
